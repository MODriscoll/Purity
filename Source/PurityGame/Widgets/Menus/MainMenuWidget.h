// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class USubMenuWidget;

class AMenuHUD;
class AMainMenuGameModeBase;

#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

/**
 * Widget designed for use with the main menu controller
 * and game mode. Can be used with a fading out animation
 * that will be called when the level has finished loading
 */
UCLASS(abstract, meta = (DisplayName = "Main Menu HUD"))
class PURITYGAME_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

	/** Called when a widget animation has finished playing */
	virtual void OnAnimationFinished_Implementation(
		const UWidgetAnimation* Animation) override;
	
protected:

	/** Called when the underlaying widget is constructed */
	virtual void NativeConstruct() override;

	/** Called when underlaying widget is destructed */
	virtual void NativeDestruct() override;

public:

	/** Set the main menu this widget belongs to */
	UFUNCTION(BlueprintCallable, Category = Menu)
	void SetMainMenu(AMainMenuGameModeBase* NewMainMenu);

	/** Set the HUD that owns this widget */
	UFUNCTION(BlueprintCallable, Category = Menu)
	void SetMenuHUD(AMenuHUD* NewMenuHUD);

	/** Get the main menu this widget belongs to */
	UFUNCTION(BlueprintPure, Category = Menu)
	AMainMenuGameModeBase* GetMainMenu() const { return MainMenu; }

	/** Get the HUD that owns this widget */
	UFUNCTION(BlueprintCallable, Category = Menu)
	AMenuHUD* GetMenuHUD() const { return MenuHUD; }

	/** Requests for the level to be loaded */
	UFUNCTION(BlueprintCallable, Category = Menu)
	void FadeOutToNewLevel(FName LevelName);

	/** Creates and opens a new sub menu */
	UFUNCTION(BlueprintCallable, Category = Menu)
	USubMenuWidget* OpenSubMenu(TSubclassOf<USubMenuWidget> Template, float Alpha = 1.f);

protected:

	/** Get the fade animation to play when closing */
	UFUNCTION(BlueprintImplementableEvent, Category = Menu)
	UWidgetAnimation* GetFadeAnimation() const;

	/** Called when the buttons should be enabled/disabled */
	UFUNCTION(BlueprintImplementableEvent, Category = Menu)
	void EnableButtons(bool bEnable);

private:

	/** Called when the level has finished loading successfully */
	UFUNCTION()
	void OnLoadSuccess(const FName& LevelName);

	/** Called when the level was failed to be loaded */
	UFUNCTION()
	void OnLoadFail(const FName& LevelName);

private:

	/** Reference to the main menu game mode */
	UPROPERTY(Transient)
	AMainMenuGameModeBase* MainMenu;

	/** Reference to the main menu HUD */
	UPROPERTY(Transient)
	AMenuHUD* MenuHUD;

	/** The name of the level provided by the main menu */
	FName LevelToOpenName;

	/** If a level has been initiated */
	uint32 bIsLevelLoading : 1;
};
