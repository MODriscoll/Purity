// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "NotificationWidget.h"

#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"

#include "WidgetTree.h"

DEFINE_LOG_CATEGORY(LogNotificationWidget);

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UNotificationWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}

const FText UNotificationListWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}


#endif

UNotificationListWidget::UNotificationListWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	FadeOutTime = 2.f;
}

void UNotificationListWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UNotificationListWidget::NativeDestruct()
{
	Super::NativeDestruct();
}

void UNotificationListWidget::NativeTick(const FGeometry& InGeometry, float InDeltaTime)
{
	Super::NativeTick(InGeometry, InDeltaTime);

	UVerticalBox* List = GetVerticalList();
	if (!List)
	{ 
		return;
	}

	for (int32 i = Notifications.Num() - 1; i >= 0; --i)
	{
		FNotificationHandle& Handle = Notifications[i];
		
		float Alpha = Handle.Update(InDeltaTime);

		// Keeping expired handles will corrupt GetNotificationCount
		if (Handle.HasExpired())
		{
			List->RemoveChild(Handle.Notification);
			Notifications.RemoveAtSwap(i);
			continue;
		}
		
		FLinearColor Colour = Handle.Notification->ColorAndOpacity;
		Colour.A = Alpha;
		Handle.Notification->SetColorAndOpacity(Colour);
	}
}

int32 UNotificationListWidget::GetNotificationCount(bool bIncludeFading) const
{
	// No expired handles should be in this array
	// since those handles are removed after updating
	int32 Count = Notifications.Num();

	if (!bIncludeFading)
	{
		for (const FNotificationHandle& Handle : Notifications)
		{
			Count -= Handle.GetState() == ENotificationState::Fading ? 1 : 0;
		}
	}

	return Count;
}

UNotificationWidget* UNotificationListWidget::PushNotification(float InLifeSpan, TSubclassOf<UNotificationWidget> Template)
{
	if (!Template)
	{
		Template = NotificationTemplate;

		// End function here if template is still not valid
		if (!Template)
		{
			UE_LOG(LogNotificationWidget, Warning, TEXT("Cannot create notification "
				"as default template was null for \"%s\""), *GetName());
			return TYPE_OF_NULLPTR();
		}
	}

	// Assure there is a valid list to add the notification to
	UVerticalBox* List = GetVerticalList();
	if (List)
	{
		UNotificationWidget* Notification =
			WidgetTree->ConstructWidget<UNotificationWidget>(Template);
		if (Notification)
		{
			// Insert the new notification at the start of the list
			List->AddChild(Notification);
			Notifications.Emplace(Notification, InLifeSpan, FadeOutTime);
		}

		return Notification;
	}
	else
	{
		UE_LOG(LogNotificationWidget, Warning, TEXT("\"%s\" does not provide a list "
			"to add new notifications to"), *GetName());
		return TYPE_OF_NULLPTR();
	}
}

#undef LOCTEXT_NAMESPACE

float FNotificationHandle::Update(float DeltaTime)
{
	StateTime += DeltaTime;

	float Alpha = 1.f;

	switch (State)
	{
		case ENotificationState::Active:
		{
			if (StateTime >= LifeSpan)
			{
				// Start the fade out now
				StateTime -= LifeSpan;
				Alpha = (FadeOutSpan - StateTime) / FadeOutSpan;
				State = ENotificationState::Fading;
			}
			break;
		}
		case ENotificationState::Fading:
		{
			if (StateTime >= FadeOutSpan)
			{
				// Keep the alpha value between 0 and 1
				StateTime -= FadeOutSpan;
				Alpha = 0.f;
				State = ENotificationState::Expired;
			}
			else
			{
				Alpha = (FadeOutSpan - StateTime) / FadeOutSpan;
			}
			break;
		}
		case ENotificationState::Expired:
		{
			Alpha = 0.f;
			break;
		}
	}

	return Alpha;
}
