// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UItemAsset;

UENUM()
enum class EInventoryInsertionType : uint8
{
	/** Each slot can only contain a unique item */
	UniqueOnly UMETA(DisplayName = "Unique"),

	/** There can be multiple slots storing the same type of item */
	DuplicateSlot UMETA(DisplayName = "Duplicate")
};

#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogInventoryComponent, Log, All);

/** Delegate for when an item has been successfully added to the inventory */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FItemAddedSignature, UItemAsset*, ItemAdded, int32, IndexInInventory);
/** Delegate for when an item has been successfully removed from the inventory */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FItemRemovedSignature, UItemAsset*, ItemRemoved, int32, IndexInInventory);

USTRUCT(BlueprintType)
struct PURITYGAME_API FInventorySlot
{
	GENERATED_BODY()

public:

	/** Defaults variables */
	FInventorySlot()
	{
		ItemReference = TYPE_OF_NULLPTR();
	}

public:

	/** Reference to an item being stored in this slot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (DisplayName = "Item"))
	UItemAsset* ItemReference;
};

USTRUCT(BlueprintType)
struct PURITYGAME_API FInventoryQuery
{
	GENERATED_BODY()

public:
	
	/** Default constructor */
	FInventoryQuery()
	{

	}

	/** Sets the items to query */
	FInventoryQuery(const TArray<UItemAsset*>& ItemsForQuery)
	{
		ItemsToQuery = ItemsForQuery;
	}

public:

	/** Add a new item to query */
	void AddQueryItem(UItemAsset* Item)
	{
		ItemsToQuery.Add(Item);
	}

	/** Returns the items that were found */
	FORCEINLINE const TArray<UItemAsset*>& GetFoundItems() const { return ItemsFound; }

	/** Returns the items that weren't found */
	FORCEINLINE const TArray<UItemAsset*>& GetMissingItems() const { return ItemsMissing; }

private:

	friend class UInventoryComponent;

	/** The items to query for */
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<UItemAsset*> ItemsToQuery;

	/** The items that were found */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<UItemAsset*> ItemsFound;

	/** The items that weren't found */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<UItemAsset*> ItemsMissing;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PURITYGAME_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	// Sets default values for this component's properties
	UInventoryComponent();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	/** Adds an new item to this inventory */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool AddItem(UItemAsset* ItemToAdd, int32& OutIndex);

	/** Query for items in this inventory */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool QueryItems(UPARAM(ref)FInventoryQuery& InOutQuery);

	/** Querys a single item by name */
	UFUNCTION(BlueprintPure, Category = "Inventory")
	bool ContainsItem(const FText& ItemName);

	/** Removes the item from the array */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveItem(int32 Index);

	/** Returns all the items in this inventor */
	FORCEINLINE const TArray<FInventorySlot>& GetStoredItems() const { return StoredItems; }

	/** Returns the max size of the inventory */
	FORCEINLINE int32 GetMaxSize() const { return MaxSize; }

public:

	/** Delegate to be called when a new item is
	*	successfully added into this inventory
	*	Called AFTER item is added
	*	@param1: The item that was added
	*	@param2: Index in the inventory the item was added
	*	Both paramaters will always be valid */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FItemAddedSignature OnItemAdded;

	/** Delegate to be called when an item is
	*	successfully removed from this inventory.
	*	Called AFTER item is removed
	*	@param1: The item that was added
	*	@param2: Index in the inventory the item was removed */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FItemRemovedSignature OnItemRemoved;

private:

	/** The items currently stored in this inventory */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true", DisplayName = "Items"))
	TArray<FInventorySlot> StoredItems;

	/** The max amount of slots this inventory stores */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true", DisplayName = "Size", ClampMin = 0))
	int32 MaxSize;
};
