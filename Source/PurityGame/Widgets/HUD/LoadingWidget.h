// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UWidgetAnimation;

#include "Blueprint/UserWidget.h"
#include "LoadingWidget.generated.h"

/**
 * Widget intended to be used for indicating to
 * the user that something is loading. Simply requires
 * an animation to play, with the animation set
 * to loop. Will start playing the animation as soon
 * as its created.
 */
UCLASS(abstract, meta = (DisplayName = "Loading Indicator"))
class PURITYGAME_API ULoadingWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

protected:

	/** Called when underlaying widget is constructed */
	virtual void NativeConstruct() override;

	/** Called when underlaying widget is destructed */
	virtual void NativeDestruct() override;

protected:

	/** Get the loading animation to play */
	UFUNCTION(BlueprintImplementableEvent, Category = Loading)
	UWidgetAnimation* GetLoadingAnimation() const;
	
};
