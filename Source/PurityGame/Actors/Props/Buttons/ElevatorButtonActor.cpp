// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ElevatorButtonActor.h"

AElevatorButtonActor::AElevatorButtonActor()
{
	PrimaryActorTick.bCanEverTick = false;

	FloorIndex = 0;
	bIsGotoButton = true;
}

#define LOCTEXT_NAMESPACE "Protagonist Focusable"

FText AElevatorButtonActor::GetFocusTip_Implementation() const
{
	FText FocusTip = FText::GetEmpty();

	int32 FloorNumber = FloorIndex + 1;
	if (bIsGotoButton)
	{
		FFormatNamedArguments Arguments;
		Arguments.Add(TEXT("FloorNumber"), FText::AsNumber(FloorNumber));
		FocusTip = FText::Format(LOCTEXT("Focusable", "Goto Floor: {FloorNumber}"), Arguments);
	}
	else
	{
		FocusTip = LOCTEXT("Focusable", "Call Elevator");
	}

	return FocusTip;
}

#undef LOCTEXT_NAMESPACE

void AElevatorButtonActor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	bAutoDisable = false;
	bShouldDelay = false;
}


