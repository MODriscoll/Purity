// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DialogueTrigger.generated.h"

UCLASS(BlueprintType, Blueprintable)
class PURITYGAME_API UDialogueScript : public UObject
{
	GENERATED_BODY()

public:

	/** Sets default values */
	//UDialogueScript();

public:
};

/** Delegate for when the script has completely finished playing */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDialogueFinishedEvent);

/**
 *	Simple class that will player dialogue when triggered 
*/
UCLASS()
class PURITYGAME_API ADialogueTrigger : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Sets default values */
	ADialogueTrigger();
};
