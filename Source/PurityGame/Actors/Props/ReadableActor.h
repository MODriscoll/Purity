// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UMessagePopupWidget;

#include "GameFramework/Actor.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "Interfaces/ProtagonistCanInteractWith.h"
#include "ReadableActor.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogReadableActor, Log, All);

UCLASS()
class PURITYGAME_API AReadableActor : public AActor, public IProtagonistCanFocusOn, public IProtagonistCanInteractWith
{
	GENERATED_BODY()
	
public:	

	/* Sets default values */
	AReadableActor();

public:

	/** Highlights this readable in the world */
	virtual void OnProtagonistFocus_Implementation(AProtagonistController* Player) override;

	/** Unhighlights this readable in the world */
	virtual void OnProtagonistUnfocus_Implementation(AProtagonistController* Player) override;

	/** Only allow readables to always be focused */
	virtual bool CanBeFocused_Implementation() const override { return true; }

	/** Inform the protagonist what this message represents */
	virtual FText GetFocusTip_Implementation() const override;

	/** Spawns the widget to represent this template and assigns the player as the owner */
	virtual void OnProtagonistInteract_Implementation(AProtagonistController* Player) override;

private:

	/** Creates the new widget and automatically adds to players viewport */
	void CreateMessageWidget(AProtagonistController* Player);

	/** Called when the popup has finished hiding */
	UFUNCTION()
	void OnMessageClosed();

public:

	/** Template for the message to be displayed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Readable)
	TSubclassOf<UMessagePopupWidget> Template;

	/** The title of this message (focus tip) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Readable)
	FText MessageTitle;

	/** The texture of the message to display to the user */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Readable)
	UTexture2D* MessageImage;

	/** If the message should be instantly toggable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Readable)
	uint32 bInstantlyToggle : 1;

	/** If the message should stall toggle requests */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Readable,
		meta = (EditCondition = "!bInstantlyToggle"))
	uint32 bStallToggleRequest : 1;

private:

	/** The mesh representing this mesh in the world */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Readable,
		meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* VisualMesh;

	/** Reference to the message widget */
	UPROPERTY(Transient)
	UMessagePopupWidget* MessageReference;
};
