// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MenuHUD.h"

#include "Widgets/Menus/MainMenuWidget.h"
#include "Widgets/Menus/SubMenuWidget.h"

DEFINE_LOG_CATEGORY(LogMenuHUD);

void AMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if (MainMenuReference || CreateMenuWidget())
	{
		MainMenuReference->AddToViewport();
		MainMenuReference->SetMenuHUD(this);
	}

	SubMenuReference = TYPE_OF_NULLPTR();
}

void AMenuHUD::SetMainMenuReference(AMainMenuGameModeBase* MainMenu)
{
	// If the menu has been created yet, create it now
	if (MainMenuReference || CreateMenuWidget())
	{
		MainMenuReference->SetMainMenu(MainMenu);
	}
}

USubMenuWidget* AMenuHUD::CreateSubMenu(TSubclassOf<USubMenuWidget> Template, float Alpha)
{
	if (!Template)
	{
		UE_LOG(LogMenuHUD, Warning, TEXT("Template for submenu is null"));
		return TYPE_OF_NULLPTR();
	}

	ClearSubMenu();

	SubMenuReference = CreateWidget<USubMenuWidget>(PlayerOwner, Template);
	
	if (MainMenuReference)
	{
		FLinearColor Color = MainMenuReference->ColorAndOpacity;
		Color.A = Alpha;
		MainMenuReference->SetColorAndOpacity(Color);
	}

	SubMenuReference->AddToViewport();
	SubMenuReference->MenuHUDReference = this;

	return SubMenuReference;
}

void AMenuHUD::ClearSubMenu()
{
	if (SubMenuReference)
	{
		SubMenuReference->RemoveFromParent();
		SubMenuReference = TYPE_OF_NULLPTR();
	}

	if (MainMenuReference)
	{
		FLinearColor Color = MainMenuReference->ColorAndOpacity;
		Color.A = 1.f;
		MainMenuReference->SetColorAndOpacity(Color);
	}
}

bool AMenuHUD::CreateMenuWidget()
{
	if (!MenuTemplate)
	{
		UE_LOG(LogMenuHUD, Warning, TEXT("No menu "
			"template has been provided for \"%s\""), *GetName());
		return false;
	}

	// Do not duplicate the menu if already created
	if (!MainMenuReference)
	{
		MainMenuReference = CreateWidget<UMainMenuWidget>(PlayerOwner, MenuTemplate);
		return MainMenuReference != TYPE_OF_NULLPTR();
	}
	else
	{
		return false;
	}
}
