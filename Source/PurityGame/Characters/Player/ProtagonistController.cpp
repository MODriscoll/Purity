// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ProtagonistController.h"

#include "Protagonist.h"
#include "ProtagonistHUD.h"

#include "Components/InteractorComponent.h"
#include "Components/InventoryComponent.h"

#include "Widgets/HUD/MapPopupWidget.h"
#include "Widgets/HUD/MinimapWidget.h"
#include "Widgets/HUD/NotificationWidget.h"

#include "Interfaces/ProtagonistCanFocusOn.h"

DEFINE_LOG_CATEGORY(LogProtagonistController);

AProtagonistController::AProtagonistController()
{
	bCanInteract = true;
	bCanUseMap = false;
	bCanMoveWhileMapWithdrawn = false;
	bInvertLookUp = false;

	ControlledProtagonist = TYPE_OF_NULLPTR();
	ProtagonistHUD = TYPE_OF_NULLPTR();
}

void AProtagonistController::AddPitchInput(float Val)
{
	Super::AddPitchInput(bInvertLookUp ? -Val : Val);
}

void AProtagonistController::Possess(APawn* inPawn)
{
	Super::Possess(inPawn);

	AProtagonist* Protagonist = Cast<AProtagonist>(GetPawn());

	// Make sure pawn being possed is the protagonist
	if (Protagonist)
	{
		ControlledProtagonist = Protagonist;

		// Bind the interaction functions
		{
			UInteractorComponent* Interactor = ControlledProtagonist->GetWorldInteractor();

			Interactor->OnObjectFocused.AddDynamic(this, &AProtagonistController::OnObjectFocused);
			Interactor->OnObjectUnfocused.AddDynamic(this, &AProtagonistController::OnObjectUnfocused);
		}

		// Bind the item inventory functions
		{
			UInventoryComponent* Inventory = ControlledProtagonist->GetItemInventory();
			Inventory->OnItemAdded.AddDynamic(this, &AProtagonistController::OnItemCollected);
			Inventory->OnItemRemoved.AddDynamic(this, &AProtagonistController::OnItemDropped);
		}
	}
	else
	{
		UE_LOG(LogProtagonistController, Warning, TEXT("Pawn being set to protagonist controller is not of protagnonist type"));
		ControlledProtagonist = TYPE_OF_NULLPTR();
	}
}

void AProtagonistController::UnPossess()
{
	Super::UnPossess();

	if (ControlledProtagonist)
	{
		// Unbind the interaction functions
		{
			UInteractorComponent* Interactor = ControlledProtagonist->GetWorldInteractor();

			Interactor->OnObjectFocused.RemoveDynamic(this, &AProtagonistController::OnObjectFocused);
			Interactor->OnObjectUnfocused.RemoveDynamic(this, &AProtagonistController::OnObjectUnfocused);
		}

		// Unbind the item inventory functions
		{
			UInventoryComponent* Inventory = ControlledProtagonist->GetItemInventory();
			Inventory->OnItemAdded.RemoveDynamic(this, &AProtagonistController::OnItemCollected);
			Inventory->OnItemRemoved.RemoveDynamic(this, &AProtagonistController::OnItemDropped);
		}
	}

	ControlledProtagonist = TYPE_OF_NULLPTR();
}

void AProtagonistController::SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning)
{
	Super::SetCinematicMode(bInCinematicMode, bHidePlayer, bAffectsHUD, bAffectsMovement, bAffectsTurning);

	if (bInCinematicMode && bAffectsHUD)
	{
		if (ProtagonistHUD)
		{
			ProtagonistHUD->HideAll();
		}

		bCanUseMap = false;
	}
	else
	{
		SetCanUseMap(true);
	}

	bCanInteract = !bInCinematicMode;
}

void AProtagonistController::BeginPlay()
{
	Super::BeginPlay();

	// Set input to intially game only
	SetInputMode(FInputModeGameOnly());
	bShowMouseCursor = false;

	ProtagonistHUD = Cast<AProtagonistHUD>(MyHUD);

	// Make sure HUD that was spawned is the protagonist hud
	if (!ProtagonistHUD)
	{
		UE_LOG(LogProtagonistController, Warning, TEXT("HUD that was set is not of Protagonist type"));
	}
}

void AProtagonistController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Bind axes
	InputComponent->BindAxis("MoveForward", this, &AProtagonistController::MoveProtagonistForward);
	InputComponent->BindAxis("MoveRight", this, &AProtagonistController::MoveProtagonistRight);

	// Bind actions
	InputComponent->BindAction("Interact", IE_Pressed, this, &AProtagonistController::Interact);
	InputComponent->BindAction("Unsheathe", IE_Pressed, this, &AProtagonistController::Unsheathe);
	InputComponent->BindAction("Pause", IE_Pressed, this, &AProtagonistController::PauseGame);
}

void AProtagonistController::MoveProtagonistForward(float Value)
{
	if (ControlledProtagonist)
	{
		// Only move when map is not withdrawn unless allowed to
		bool bMapWithdrawn = ProtagonistHUD && ProtagonistHUD->IsMessageVisible();
		if (!bMapWithdrawn || bCanMoveWhileMapWithdrawn)
		{
			ControlledProtagonist->MoveForward(Value);
		}
	}
}

void AProtagonistController::MoveProtagonistRight(float Value)
{
	if (ControlledProtagonist)
	{
		// Only move when map is not withdrawn unless allowed to
		bool bMapWithdrawn = ProtagonistHUD && ProtagonistHUD->IsMessageVisible();
		if (!bMapWithdrawn || bCanMoveWhileMapWithdrawn)
		{
			ControlledProtagonist->MoveRight(Value);
		}
	}
}

void AProtagonistController::Interact()
{
	if (ControlledProtagonist && bCanInteract)
	{
		// If a message is open, close it instead of interacting
		if (ProtagonistHUD && ProtagonistHUD->IsMessageVisible())
		{
			// Don't close if message is the map
			if (!ProtagonistHUD->IsMessageTheMap())
			{
				ProtagonistHUD->HideMessage(false);
			}
		}
		else
		{
			ControlledProtagonist->Interact(this);

			// If an object is being focused on, get its
			// focus tip again, as the tip may have changed
			AActor* FocusedObject = ControlledProtagonist->GetWorldInteractor()->GetFoundInteractive();
			if (FocusedObject)
			{
				OnObjectFocused(FocusedObject);
			}
		}
	}
}

void AProtagonistController::Unsheathe()
{
	if (ProtagonistHUD)
	{
		// If the player does not have the map,
		// check if they have picked it up now
		if (!bCanUseMap && !bCinematicMode)
		{
			SetCanUseMap(true);
		}

		if (bCanUseMap)
		{
			// Only update the protagonists position
			// when the map is fully hiden
			if (!ProtagonistHUD->IsMessageVisible())
			{
				ProtagonistHUD->SetProtagonistMarkerPosition(ControlledProtagonist->GetActorLocation());
			}

			ProtagonistHUD->ToggleMap();
		}
		else
		{
			// Only allow map to be hidden
			ProtagonistHUD->HideMessage(false);
		}
	}
}

void AProtagonistController::PauseGame()
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->TogglePauseMenu();
	}
}

void AProtagonistController::SetCanUseMap(bool bCan)
{
	if (bCan)
	{
		bCan = HasMapItem();
	}
	
	bCanUseMap = bCan;
}

void AProtagonistController::SetFocusTip(const FText& Tip)
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->SetFocusTip(Tip);
	}
}

UNotificationWidget* AProtagonistController::PushNotification(
	float InLifeSpan, TSubclassOf<UNotificationWidget> Template)
{
	if (ProtagonistHUD)
	{
		return ProtagonistHUD->PushNotification(InLifeSpan, Template);
	}

	return TYPE_OF_NULLPTR();
}

UNotificationWidget * AProtagonistController::PushNotification(const FText& Notification,
	UTexture2D* Symbol, float InLifeSpan, TSubclassOf<UNotificationWidget> Template)
{
	if (ProtagonistHUD)
	{
		UNotificationWidget* NotifyWidget = ProtagonistHUD->PushNotification(InLifeSpan, Template);
		// Assure the notification was valid
		if (NotifyWidget)
		{
			NotifyWidget->Notification = Notification;
			NotifyWidget->Symbol = Symbol;
		}

		return NotifyWidget;
	}

	return TYPE_OF_NULLPTR();
}

#define LOCTEXT_NAMESPACE "Interactive"

bool AProtagonistController::HasMapItem() const
{
	if (ControlledProtagonist)
	{
		UInventoryComponent* KeyItems = ControlledProtagonist->GetKeyItemInventory();
		return KeyItems->ContainsItem(LOCTEXT("Items", "Map"));
	}

	return false;
}

#undef LOCTEXT_NAMESPACE

void AProtagonistController::OnObjectFocused(AActor* AsActor)
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->SetFocusTip(IProtagonistCanFocusOn::Execute_GetFocusTip(AsActor));
	}
}

void AProtagonistController::OnObjectUnfocused(AActor* AsActor)
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->SetFocusTip(FText::GetEmpty());
	}
}

void AProtagonistController::OnItemCollected(UItemAsset* ItemAdded, int32 IndexInInventory)
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->ToggleInventoryItemIcon(ItemAdded, IndexInInventory);
	}
}

void AProtagonistController::OnItemDropped(UItemAsset* ItemAdded, int32 IndexInInventory)
{
	if (ProtagonistHUD)
	{
		ProtagonistHUD->ToggleInventoryItemIcon(TYPE_OF_NULLPTR(), IndexInInventory);
	}
}