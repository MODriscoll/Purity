// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UImage;
class UTextBlock;

#include "Blueprint/UserWidget.h"
#include "MarkerWidget.generated.h"

/**
 * Base class for marker like widget. Allows
 * C++ classes to request an image from the
 * derived blueprint
 */
UCLASS(abstract, meta = (DisplayName = "Marker Icon"))
class PURITYGAME_API UMarkerWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

public:

	/** Get the image associated with this marker */
	UFUNCTION(BlueprintImplementableEvent, Category = Marker)
	UImage* GetMarkerImage() const;	
};
