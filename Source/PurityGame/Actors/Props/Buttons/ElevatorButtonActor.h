// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Props/ButtonActor.h"
#include "ElevatorButtonActor.generated.h"

/**
* A button thats designed to work with elevators
*/
UCLASS()
class PURITYGAME_API AElevatorButtonActor : public AButtonActor
{
	GENERATED_BODY()

public:

	/** Sets the default value for this actor */
	AElevatorButtonActor();

public:

	/** Tells the protagonist what floor this
	button will inform the elevator to goto */
	virtual FText GetFocusTip_Implementation() const override;

	/** Called when actor is spawned or placed in editor */
	virtual void OnConstruction(const FTransform& Transform) override;

public:

	/** The floor index this button will represent */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Elevator")
	int32 FloorIndex;

	/** If this button is a home button or a goto button */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elevator")
	uint32 bIsGotoButton : 1;
};
