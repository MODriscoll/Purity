// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FlashingLightActor.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogFlashingLightActor, Log, All);

USTRUCT(BlueprintType)
struct PURITYGAME_API FFlashSegment
{
	GENERATED_BODY()

public:

	/** Set default value */
	FFlashSegment()
	{
		IntensityRangeStart = 0.f;
		IntensityRangeEnd = 100000.f;
		bUseRandomRange = true;
		LastsFor = 0.1f;
	}

public:

	/** The start of the intensity range for this segment */
	UPROPERTY(EditAnywhere, Interp, BlueprintReadWrite, Category = "Flickering|Segment", meta = (ClampMin = "0", ClampMax = "100000"))
	float IntensityRangeStart;

	/** The end of the intensity range for this segment */
	UPROPERTY(EditAnywhere, Interp, BlueprintReadWrite, Category = "Flickering|Segment", meta = (ClampMin = "0", ClampMax = "100000"))
	float IntensityRangeEnd;

	/** If a random values between the start and end should be chosen */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flickering|Segment")
	uint32 bUseRandomRange : 1;

	/** How long this segment will last for */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flickering|Segment", meta = (SliderExponent = "0.1"))
	float LastsFor;
};

USTRUCT()
struct FFlashSegmentData
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FFlashSegmentData()
	{
		AlphaValue = 0.f;
		StartIntensity = 0.f;
		FinalIntensity = 100000.f;
	}


public:

	/** The starting intensity for this flashs segment */
	float StartIntensity;

	/** The final intensity for this flashs segment */
	float FinalIntensity;

	/** The current alpha (lerped value) of this segment */
	float AlphaValue;
};

UCLASS()
class PURITYGAME_API AFlashingLightActor : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Sets default values for this actor's properties */
	AFlashingLightActor();

public:

	/** Called every frame */
	virtual void Tick(float DeltaTime) override;

	/** Called to check if this actor can be ticked in the editor */
	virtual bool ShouldTickIfViewportsOnly() const override { return FlickerSegments.Num() != 0; }


protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

#if WITH_EDITOR

	/** Called when a property has been changed */
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& ProperptyChangedEvent) override;

#endif

private:

	/** Prepares the next segment to be used */
	void InitNextSegment();

	/** Sets the segment data using the given index */
	const FFlashSegmentData& GetNewSegmentData(int32 Index);

	/** Updates the segment being used */
	int32 GetNewIndex();

public:

	/** If the flicker segments should be followed in order and loop */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Flickering)
	uint32 bInOrder : 1;

	/** If the same segment should be used twice in a row if random */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Flickering, meta = (EditCondition = "!bInOrder"))
	uint32 bRepeatSegment : 1;

	/** If the segmenter is paused or not. If paused 
	the insensity will not be updated, meaning the insensity
	of the light can be set to the value you need */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Flickering)
	uint32 bIsPaused : 1;

private:

	/** Index of the current segment being used */
	int32 SegmentIndex;	

	/** Data obtained from the current segment to be used to flicker the light */
	FFlashSegmentData SegmentData;

	/** The segments this light will use to flicker */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Flickering, meta = (AllowPrivateAccess = "true"))
	TArray<FFlashSegment> FlickerSegments;

	/** The light compoenent to acts as this actors base */
	UPROPERTY(Category = Flickering, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPointLightComponent* FlickeringLight;

	/** The randomiser stream for random selection of segments */
	UPROPERTY(Category = Flickering, EditInstanceOnly, meta = (AllowPrivateAccess = "true"))
	FRandomStream SegmentRandomiser;
};

