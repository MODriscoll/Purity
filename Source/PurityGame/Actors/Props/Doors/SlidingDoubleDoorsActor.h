// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Props/DoorActor.h"
#include "SlidingDoubleDoorsActor.generated.h"

UENUM(BlueprintType)
enum class EDoorSlideAxis : uint8
{
	X,
	Z
};

/**
* A frame with two sliding doors that slide away from
* each other to open and slide towards to close. This
* door is suitable for elevator like doors.
*/
UCLASS()
class PURITYGAME_API ASlidingDoubleDoorsActor : public ADoorActor
{
	GENERATED_BODY()
	
public:

	/** Sets the default values */
	ASlidingDoubleDoorsActor();
	
protected:

#if WITH_EDITOR

	/** Called when a property has been changed in the editor */
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif

	/** Slides the doors from the base of the door based on the alpha value */
	virtual void TickDoorMotion(float Alpha) override;

public:

	/** Set the axis to slide along, will appropriately
	rotate both of the sliding doors as well */
	UFUNCTION(BlueprintCallable, Category = Door)
	void SetSlideAxis(EDoorSlideAxis Axis);

public:

	/** The maximum offset from the center of the door that each door can go */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door, meta = (DisplayName = "Max Spread"))
	float MaxOffset;

private:

	/** The base (center) of the door, always points facing forward */
	UPROPERTY(Category = Door, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* DoorBase;

	/** The mesh for the door that will be moving negatively along the axis */
	UPROPERTY(Category = Door, VisibleAnywhere, BlueprintReadOnly, 
		meta = (AllowPrivateAccess = "true", DisplayName = "Negative Door"))
	UStaticMeshComponent* NegativeDoorMesh;

	/** The mesh for the door that will be moving positively along the axis */
	UPROPERTY(Category = Door, VisibleAnywhere, BlueprintReadOnly, 
		meta = (AllowPrivateAccess = "true", DisplayName = "Positive Door"))
	UStaticMeshComponent* PositiveDoorMesh;

	/** The axis to slide the doors along */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Door,
		meta = (AllowPrivateAccess = "true"))
	EDoorSlideAxis SlideAxis;
};
