// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ProtagonistCanFocusOn.h"


// This function does not need to be modified.
UProtagonistCanFocusOn::UProtagonistCanFocusOn(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IProtagonistCanFocusOn functions that are not pure virtual.
