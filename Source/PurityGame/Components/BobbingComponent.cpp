// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "BobbingComponent.h"

DEFINE_LOG_CATEGORY(LogBobbingComponent);

UBobbingComponent::UBobbingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	// Default values for bobbing
	BobbingSpeed = 10.f;
	BobbingDiameter = 10.f;
	bAutoCenter = true;

	// This component should not be visible
	bVisible = false;
}

void UBobbingComponent::BeginPlay()
{
	Super::BeginPlay();

	Reset();
}

void UBobbingComponent::OnAttachmentChanged()
{
	Reset();
}

void UBobbingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (DeltaInput != 0.f)
	{
		// Updated the running time using the input delta
		UpdateRunningTime(ConsumeInput(), DeltaTime);
	}
	else if (bAutoCenter)
	{
		// Get the running time to equal a value that resets
		// the components position to the 'center'
		// ( 0 or PI * 2.f and PI both return 0 from Sin)
		if (!FMath::IsNearlyEqual(RunningTime, (PI + HALF_PI) + (HALF_PI * ApexSignValue) - PI, 0.15f))
		{
			UpdateRunningTime(1.f, DeltaTime);
		}
	}
}

void UBobbingComponent::AddInput(float Value)
{
	DeltaInput += Value;
}

void UBobbingComponent::Reset()
{
	RunningTime = 0.f;
	DeltaInput = 0.f;

	// Set the apex sign based on which way speed is going
	SetSpeed(BobbingSpeed);
	
	// TODO: Make socket system similar to spring arm component
	//SetRelativeLocation(FVector::ZeroVector);
}

void UBobbingComponent::SetSpeed(float NewSpeed)
{
	BobbingSpeed = NewSpeed;
	ApexSignValue = -FMath::Sign(BobbingSpeed);
}

float UBobbingComponent::ConsumeInput()
{
	float Temp = DeltaInput;
	DeltaInput = 0.f;
	return Temp;
}

EBobbingApex UBobbingComponent::GetBobbingApex() const
{
	EBobbingApex Apex = EBobbingApex::ApexLow;

	if (ApexSignValue < 0)
	{
		Apex = EBobbingApex::ApexHigh;
	}

	return Apex;
}

void UBobbingComponent::UpdateRunningTime(float Scale, float DeltaTime)
{
	// Get the new and old running time
	float Input = ConsumeInput();
	float PreviousTime = RunningTime;
	RunningTime += Scale * (BobbingSpeed * DeltaTime);

	// Increment the vertical positon of this component
	float Delta = FMath::Sin(RunningTime) - FMath::Sin(PreviousTime);
	FVector NewLocation = GetComponentLocation();
	NewLocation.Z += BobbingDiameter * Delta;
	SetWorldLocation(NewLocation);

	// Update the tracking values
	UpdateApexSign();
}

void UBobbingComponent::UpdateApexSign()
{
	// Is the run time nearly at the apex?
	// ( Apexes = HALF_PI or PI + HALF_PI )
	if (FMath::IsNearlyEqual(RunningTime, PI + (HALF_PI * ApexSignValue), 0.15f))
	{
		// Call the on apex reached event
		EBobbingApex Apex = GetBobbingApex();
		OnApexReached.Broadcast(Apex);

		ApexSignValue = -ApexSignValue;
	}

	// Loop running time to stay within unit circle in radians
	RunningTime = FMath::Fmod(RunningTime, PI * 2.f);
}


