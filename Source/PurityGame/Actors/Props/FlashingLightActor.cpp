// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "FlashingLightActor.h"

#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY(LogFlashingLightActor);

AFlashingLightActor::AFlashingLightActor()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set default values
	bInOrder = true;
	bRepeatSegment = false;
	bIsPaused = false;

	// Create the flashing light and set it as the root
	FlickeringLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("Flickering Light"));
	RootComponent = FlickeringLight;
}

void AFlashingLightActor::BeginPlay()
{
	Super::BeginPlay();
	// Make sure there is at least one segment before setting index
	// If there are no segments, index will be set to -1
	SegmentIndex = !FlickerSegments.Num() ? -1 : bInOrder ? 0 : SegmentRandomiser.RandRange(0, FlickerSegments.Num() - 1);
}

#if WITH_EDITOR

void AFlashingLightActor::PostEditChangeChainProperty(FPropertyChangedChainEvent& ProperptyChangedEvent)
{
	Super::PostEditChangeChainProperty(ProperptyChangedEvent);

	// Make sure the flash segments min and max values stay in appropriate range
	const FName PropertyName = ProperptyChangedEvent.Property ? ProperptyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(AFlashingLightActor, FlickerSegments))
	{
		int32 Index = ProperptyChangedEvent.GetArrayIndex(PropertyName.ToString());
		if (FlickerSegments.IsValidIndex(Index))
		{
			FFlashSegment& Segment = FlickerSegments[Index];
			Segment.IntensityRangeStart = FMath::Clamp(Segment.IntensityRangeStart, 0.f, 100000.f);
			Segment.IntensityRangeEnd = FMath::Clamp(Segment.IntensityRangeEnd, 0.f, 100000.f);
			Segment.LastsFor = FMath::Max(Segment.LastsFor, KINDA_SMALL_NUMBER);
			if (SegmentIndex == -1)
			{
				InitNextSegment();
			}
		}
		else
		{
			if (FlickerSegments.Num() > 0)
			{
				SegmentIndex = 0;
				InitNextSegment();
			}
		}
	}
}

#endif

void AFlashingLightActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Make sure current segment is valid
	if (!bIsPaused && SegmentIndex >= 0)
	{
		SegmentData.AlphaValue += (DeltaTime / FlickerSegments[SegmentIndex].LastsFor);
		SegmentData.AlphaValue = FMath::Clamp(SegmentData.AlphaValue, 0.f, 1.f);
		FlickeringLight->SetIntensity(FMath::Lerp(SegmentData.StartIntensity,
			SegmentData.FinalIntensity, SegmentData.AlphaValue));

		// If this segment is done, prepare the
		// next segment for the next frame
		if (SegmentData.AlphaValue == 1.f)
		{
			InitNextSegment();
		}
	}
}

void AFlashingLightActor::InitNextSegment()
{
	GetNewSegmentData(GetNewIndex());
}

const FFlashSegmentData& AFlashingLightActor::GetNewSegmentData(int32 Index)
{
	if (FlickerSegments.IsValidIndex(Index))
	{
		const FFlashSegment& Segment = FlickerSegments[Index];
		FFlashSegmentData Data;

		if (Segment.bUseRandomRange)
		{
			float Sign = FMath::Sign(Segment.IntensityRangeEnd - Segment.IntensityRangeStart);

			// Need to make sure the lower value is on the left
			if (Sign >= 0.f)
			{
				Data.StartIntensity = SegmentRandomiser.FRandRange(Segment.IntensityRangeStart, Segment.IntensityRangeEnd);
				Data.FinalIntensity = SegmentRandomiser.FRandRange(Data.StartIntensity, Segment.IntensityRangeEnd);
			}
			else
			{
				Data.StartIntensity = SegmentRandomiser.FRandRange(Segment.IntensityRangeEnd, Segment.IntensityRangeStart);
				Data.FinalIntensity = SegmentRandomiser.FRandRange(Data.StartIntensity, Segment.IntensityRangeStart);
			}
		}
		else
		{
			Data.StartIntensity = Segment.IntensityRangeStart;
			Data.FinalIntensity = Segment.IntensityRangeEnd;
		}

		return SegmentData = Data;
	}
	else
	{
		return SegmentData;
	}
}

int32 AFlashingLightActor::GetNewIndex()
{
	// To prevent division by zero incase there are no segments
	if (!FlickerSegments.Num())
	{
		return SegmentIndex = -1;
	}

	if (bInOrder)
	{
		// Keep the index within range of the array
		return (++SegmentIndex) %= FlickerSegments.Num();
	}
	else
	{
		// Generate a new random number within range
		int32 NewIndex = SegmentRandomiser.RandRange(0, FlickerSegments.Num() - 1);
		if (!bRepeatSegment && NewIndex == SegmentIndex)
		{
			return (++SegmentIndex) %= FlickerSegments.Num();
		}

		return SegmentIndex = NewIndex;
	}
}


