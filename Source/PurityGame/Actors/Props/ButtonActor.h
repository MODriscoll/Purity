// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AButtonActor;

#include "GameFramework/Actor.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "Interfaces/ProtagonistCanInteractWith.h"
#include "ButtonActor.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogButtonActor, Log, All);

/** Delegate for when the button has been pressed */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FButtonPressSignature, AButtonActor*, Button, AActor*, PressedBy);

/** Delegate for when the button has been activated */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FButtonActivationSignature);

UCLASS()
class PURITYGAME_API AButtonActor : public AActor, public IProtagonistCanFocusOn, public IProtagonistCanInteractWith
{
	GENERATED_BODY()
	
public:	

	/** Sets default values for this actor's properties */
	AButtonActor();

public:

	/** Highlights this button in the world */
	virtual void OnProtagonistFocus_Implementation(AProtagonistController* Player) override;

	/** Unhighlights this button in the world */
	virtual void OnProtagonistUnfocus_Implementation(AProtagonistController* Player) override;

	/** Buttons are only focusable if its not being delayed */
	virtual bool CanBeFocused_Implementation() const { return bCanBePressed; }

	/** Gives the protagonist a tip about using this button */
	virtual FText GetFocusTip_Implementation() const;

	/** Simulates a button press using the player */
	virtual void OnProtagonistInteract_Implementation(AProtagonistController* Player) override;

public:

	/** Simulates a button press */
	UFUNCTION(BlueprintCallable, Category = Button)
	void Press(AActor* PressedBy);

	/** Set if this button can be pressed */
	UFUNCTION(BlueprintCallable, Category = Button)
	void SetCanBePressed(bool bCan, bool bDelay = false);

	/** Get if this button can be pressed */
	bool CanBePressed() const { return bCanBePressed; }

	/** Get the mesh of the button */
	UStaticMeshComponent* GetButtonMesh() const { return ButtonMesh; }

private:

	/** Called after the delay has finished */
	UFUNCTION()
	void Activate();

public:

	/** Delegate to be called when the button was successfully pressed 
		@parma1: Reference to this button
		@param2: The actor who pushed the button */
	UPROPERTY(BlueprintAssignable, Category = Button)
	FButtonPressSignature OnButtonPressAccepted;

	/** Delegate to be called when the button was unsuccessfully pressed
		@parma1: Reference to this button
		@param2: The actor who pushed the button */
	UPROPERTY(BlueprintAssignable, Category = Button)
	FButtonPressSignature OnButtonPressDenied;

	/** Delegate for when the button has been
	activated after being in an inactive state */
	UPROPERTY(BlueprintAssignable, Category = Button)
	FButtonActivationSignature OnButtonActivated;

	/** If this button should disable itself after a successfull press */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Button, meta = (DisplayName = "Automatically Disable"))
	uint32 bAutoDisable : 1;

	/** If this button should delay between each press */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Button, meta = (EditCondition = "bAutoDisable"))
	uint32 bShouldDelay : 1;

	/** How long the delay should last for */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Button, meta = (EditCondition = "bShouldDelay"))
	float DelayFor;

private:

	/** If the is currently delayed */
	UPROPERTY(Category = Button, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	uint32 bCanBePressed : 1;

	/** The button the player is able to interact with */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Button, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ButtonMesh;

	/** The handle for the delay between presses */
	FTimerHandle DelayHandle;
};
