// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "PauseMenuWidget.h"

#include "WidgetAnimation.h"

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UPauseMenuWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}

#endif

void UPauseMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	// We are only concerned for the fade animation.
	// Don't call the events on other animations
	if (Animation == GetFadeAnimation())
	{
		if (bIsClosing)
		{
			OnFadeOutFinished.Broadcast();
		}
		else
		{
			EnableButtons(true);
		}
	}
}

void UPauseMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsClosing = false;

	// If no fade animation is provided, skip straight to enabling buttons
	UWidgetAnimation* FadeAnimation = GetFadeAnimation();
	if (FadeAnimation)
	{
		PlayAnimation(FadeAnimation, 0.f, 1, EUMGSequencePlayMode::Forward);
	}

	EnableButtons(!FadeAnimation);
}

bool UPauseMenuWidget::IsFading() const
{
	return IsAnimationPlaying(GetFadeAnimation());
}

void UPauseMenuWidget::ClosePauseMenu()
{
	if (!bIsClosing)
	{
		EnableButtons(false);
		bIsClosing = true;

		UWidgetAnimation* FadeAnimation = GetFadeAnimation();
		if (FadeAnimation)
		{
			PlayAnimation(FadeAnimation, 0.f, 1, EUMGSequencePlayMode::Reverse);
			OnFadeOutStart.Broadcast();
		}
		else
		{
			OnFadeOutFinished.Broadcast();
		}
	}
}

#undef LOCTEXT_NAMESPACE


