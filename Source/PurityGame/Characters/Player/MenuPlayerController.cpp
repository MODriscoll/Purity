// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MenuPlayerController.h"

#include "MenuHUD.h"
#include "GameModes/Menu/MainMenuGameModeBase.h"
#include "Widgets/Menus/MainMenuWidget.h"

DEFINE_LOG_CATEGORY(LogMenuController);

void AMenuPlayerController::ReceivedGameModeClass(TSubclassOf<AGameModeBase> GameModeClass)
{
	Super::ReceivedGameModeClass(GameModeClass);

	MenuGameMode = Cast<AMainMenuGameModeBase>(GameModeClass.Get());

	check(MenuGameMode);
}

void AMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// Disable game related input
	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;

	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		ControlledPawn->SetActorHiddenInGame(true);
	}

	MenuHUD = Cast<AMenuHUD>(MyHUD);

	// Make sure the HUD that as spawned was the menu hud
	if (!MenuHUD)
	{
		UE_LOG(LogMenuController, Warning, TEXT("HUD that was set is not of Menu type"));
	}
	else
	{
		MenuGameMode = Cast<AMainMenuGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		MenuHUD->SetMainMenuReference(MenuGameMode);
	}
}
