// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "SoundEmitterActor.h"

DEFINE_LOG_CATEGORY(LogSoundEmitterActor);

// Sets default values
ASoundEmitterActor::ASoundEmitterActor()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default values
	InitialDelay = 5.f;
	bInOrder = true;
	bRepeatSegment = false;

	// Create the emitters source and set it as the root
	EmitterSource = CreateDefaultSubobject<UAudioComponent>(TEXT("Sound Source"));
	RootComponent = EmitterSource;

	// Set up override attenuation event though we don't use it by default
	EmitterSource->bOverrideAttenuation = false;
	FSoundAttenuationSettings& Attenuation = EmitterSource->AttenuationOverrides;
	Attenuation.DistanceAlgorithm = EAttenuationDistanceModel::Linear;
	Attenuation.AttenuationShape = EAttenuationShape::Box;
	Attenuation.AttenuationShapeExtents = FVector(200.f, 200.f, 100.f);
	Attenuation.FalloffDistance = 600.f;

	// Bind the delay to be set when the current sound finishes
	EmitterSource->OnAudioFinished.AddDynamic(this, &ASoundEmitterActor::DelayNextSegment);
}

void ASoundEmitterActor::BeginPlay()
{
	Super::BeginPlay();
	
	if (InitialDelay > 0.f)
	{
		FTimerManager& Manager = GetWorldTimerManager();
		Manager.SetTimer(DelayHandle, this, 
			&ASoundEmitterActor::PlayNextSegment, InitialDelay, false);
	}
	else
	{
		PlayNextSegment();
	}
}

void ASoundEmitterActor::PlayNextSegment()
{
	if (SoundSegments.Num() < 1)
	{
		UE_LOG(LogSoundEmitterActor, Warning, TEXT("\"%s\" has no sounds to emit"), *GetName());
		return;
	}

	if (bInOrder)
	{
		(++SegmentIndex) %= SoundSegments.Num();
	}
	else
	{
		int32 RandomIndex = SegmentRandomiser.RandRange(0, SoundSegments.Num() - 1);
		// Make sure the segment doesn't repeat if requested
		if (!bRepeatSegment && RandomIndex == SegmentIndex)
		{
			(++RandomIndex) %= SoundSegments.Num();
		}

		SegmentIndex = RandomIndex;
	}

	FSoundSegment& Segment = SoundSegments[SegmentIndex];
	if (Segment.Sound)
	{
		EmitterSource->SetSound(Segment.Sound);

		// If sound didn't auto play, play it now
		if (!EmitterSource->IsPlaying())
		{
			EmitterSource->Play();
		}
	}
	else
	{
		UE_LOG(LogSoundEmitterActor, Warning,
			TEXT("The sound for segment index %i in %s has not been set"), SegmentIndex, *GetName());
	}
}

void ASoundEmitterActor::DelayNextSegment()
{
	if (SoundSegments.IsValidIndex(SegmentIndex))
	{
		FTimerManager& Manager = GetWorldTimerManager();
		FSoundSegment& Segment = SoundSegments[SegmentIndex];
		Manager.SetTimer(DelayHandle, this, 
			&ASoundEmitterActor::PlayNextSegment, Segment.DelayAfter, false);
	}
}

