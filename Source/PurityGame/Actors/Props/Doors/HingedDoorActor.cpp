// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "HingedDoorActor.h"

#include "Characters/Player/Protagonist.h"
#include "Characters/Player/ProtagonistController.h"

AHingedDoorActor::AHingedDoorActor()
{
	PrimaryActorTick.bCanEverTick = false;

	MaxRotation = 90.f;
	SwingMode = EHingeType::Push;
	Hinge = TYPE_OF_NULLPTR();
}

#define LOCTEXT_NAMESPACE "Protagonist Focusable"

FText AHingedDoorActor::GetFocusTip_Implementation() const
{
	FText FocusTip = FText::GetEmpty();

	if (IsLocked())
	{
		FocusTip = LOCTEXT("Focusable", "Locked...");
	}
	else
	{
		if (IsOpen())
		{
			FocusTip = LOCTEXT("Focusable", "Interact to Close");
		}
		else
		{
			FocusTip = LOCTEXT("Focusable", "Interact To Open");
		}
	}

	return FocusTip;
}

#undef LOCTEXT_NAMESPACE

void AHingedDoorActor::BeginPlay()
{
	// Setting the desired rotation before calling super, 
	// this is so if hinge can be set properly during startup
	DesiredRotation = MaxRotation;
	Super::BeginPlay();
}

void AHingedDoorActor::StartOpenDoor(const AActor* OpenedBy)
{
	if (Hinge)
	{
		Super::StartOpenDoor(OpenedBy);

		float Sign = 1.f;

		if (SwingMode != EHingeType::AlwaysPush &&
			SwingMode != EHingeType::AlwaysPull)
		{
			Sign = SwingMode == EHingeType::Push ? 1.f : -1.f;
		}
		else
		{
			//TODO: Improve calculation to work based off doors up XY plane
			// This calculations assumes doors up vector is always (0, 0, 1)
			if (OpenedBy)
			{
				FVector Direction = GetActorLocation() - OpenedBy->GetActorLocation();
				Direction.Normalize();

				float Dot = FVector::DotProduct(Hinge->GetForwardVector(), Direction);
				float DotSign = FMath::Sign(Dot);

				// Don't allow the sign to be zero,
				// if its zero, the door won't rotate
				if (DotSign != 0)
				{
					Sign = SwingMode == EHingeType::AlwaysPush ? DotSign : -DotSign;
				}
			}
			else
			{
				Sign = SwingMode == EHingeType::AlwaysPush ? 1.f : -1.f;
			}
		}

		DesiredRotation = MaxRotation * Sign;
	}
}

void AHingedDoorActor::TickDoorMotion(float Alpha)
{
	if (Hinge)
	{
		// Set the hinge rotation based of alpha value
		FRotator NewRotation = FMath::Lerp(FRotator(0.f), FRotator(0.f, DesiredRotation, 0.f), FMath::Clamp(Alpha, 0.f, 1.f));
		Hinge->SetRelativeRotation(NewRotation);
	}
}



