// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ProtagonistHUD.h"

#include "Widgets/HUD/FocusTipWidget.h"
#include "Widgets/HUD/NotificationWidget.h"
#include "Widgets/HUD/MapPopupWidget.h"
#include "Widgets/Menus/PauseMenuWidget.h"

#include "Widgets/HUD/MinimapWidget.h"
#include "Widgets/HUD/InventoryWidget.h"

#include "WidgetAnimation.h"

#include "WidgetBlueprintLibrary.h"

DEFINE_LOG_CATEGORY(LogProtagonistHUD);

AProtagonistHUD::AProtagonistHUD()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set default variables
	bDrawCrosshair = true;
	bHandlePauseInputSettings = true;
	bTrackProtagonist = false;

	Crosshair = GetDefaultCrosshair();

	// Create a scene component to use as a dummy root
	USceneComponent* DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy"));
	RootComponent = DummyRoot;

	// Create the message audio and attach it to the dummy
	MessageAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Message Sound Effects"));
	MessageAudio->SetupAttachment(DummyRoot);
		
	// Set up the popup audio to work in 2D space
	MessageAudio->bIsUISound = true;
	MessageAudio->bAllowSpatialization = false;
	MessageAudio->bAutoDestroy = false;

	// Create the pause audio and attach it to the dummy
	PauseAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("Pause Sound Effects"));
	PauseAudio->SetupAttachment(DummyRoot);

	// Set up the pause audio to work in 2D space
	PauseAudio->bIsUISound = true;
	PauseAudio->bAllowSpatialization = false;
	PauseAudio->bAutoDestroy = false;
}

void AProtagonistHUD::BeginPlay()
{
	Super::BeginPlay();

	CreateMapWidget();

	// Add to viewport now, so widgets construct
	// events get called at the start of the game
	if (MapReference)
	{
		MapReference->AddToViewport();
		MapReference->Hide(true, true);
	}

	PauseReference = TYPE_OF_NULLPTR();
}

void AProtagonistHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (EndPlayReason == EEndPlayReason::Destroyed && GEngine)
	{
		GEngine->bSubtitlesForcedOff = false;
	}
}

void AProtagonistHUD::DrawHUD()
{
	Super::DrawHUD();

	float X, Y;
	Canvas->GetCenter(X, Y);

	// Only draw the hud elements while unpaused
	// and the map has not been withdrawn
	bool bCanDraw = MapReference ? !IsMessageVisible() : true;
	bCanDraw &= !PauseReference;

	if (Crosshair && bDrawCrosshair)
	{
		if (bCanDraw)
		{
			DrawTexture(Crosshair, X, Y, Crosshair->GetSizeX(), Crosshair->GetSizeY(), 0.f, 0.f, 1.f, 1.f);
		}
	}

	// Only draw tips and notifications if no message is being displayed
	ESlateVisibility Visibility = bCanDraw && bShowHUD ? ESlateVisibility::Visible : ESlateVisibility::Hidden;

	if (TipReference)
	{
		TipReference->SetVisibility(Visibility);
	}

	if (NotifyListReference)
	{
		if (NotifyListReference->GetNotificationCount() > 0)
		{
			NotifyListReference->SetVisibility(Visibility);
		}
		else
		{
			NotifyListReference->RemoveFromParent();
			NotifyListReference = TYPE_OF_NULLPTR();
		}
	}
}

void AProtagonistHUD::ShowHUD()
{
	Super::ShowHUD();

	if (!bShowHUD)
	{
		HideMessage();
	}

	SetWidgetVisibility(TipReference);
	SetWidgetVisibility(NotifyListReference);
}

void AProtagonistHUD::SetFocusTip(const FText& Tip)
{
	if (!TipTemplate)
	{
		UE_LOG(LogProtagonistHUD, Warning, TEXT("No tip "
			"template has been provided for \"%s\""), *GetName());
		return;
	}

	if (Tip.IsEmpty())
	{
		// Make sure to remove the tip from the viewport
		if (TipReference && TipReference->IsInViewport())
		{
			TipReference->RemoveFromParent();
		}

		TipReference = TYPE_OF_NULLPTR();
	}
	else 
	{
		if (TipReference)
		{
			TipReference->FocusTip = Tip;
		}
		else
		{
			TipReference = CreateWidget<UFocusTipWidget>(PlayerOwner, TipTemplate);
			TipReference->FocusTip = Tip;
			TipReference->AddToViewport();

			// Set the visibility of the tip now, as DrawHUD
			// does not get called when not showing HUD
			SetWidgetVisibility(TipReference);
		}
	}
}

UNotificationWidget* AProtagonistHUD::PushNotification(float LifeSpan, TSubclassOf<UNotificationWidget> Template)
{
	if (LifeSpan <= 0.f)
	{
		return TYPE_OF_NULLPTR();
	}

	if (!NotifyListReference)
	{
		if (!NotifyListTemplate)
		{
			UE_LOG(LogProtagonistHUD, Warning, TEXT("No notification list "
				"template has been provided for \"%s\""), *GetName());
			return TYPE_OF_NULLPTR();
		}

		// Add the list to the viewport immediately
		NotifyListReference = CreateWidget<UNotificationListWidget>(PlayerOwner, NotifyListTemplate);
		NotifyListReference->AddToViewport();

		// Set the visibility of the tip now, as DrawHUD
		// does not get called when not showing HUD
		SetWidgetVisibility(NotifyListReference);
	}

	return NotifyListReference->PushNotification(LifeSpan, Template);
}

void AProtagonistHUD::ToggleMap()
{
	// If map doesn't exist, try making it now
	if (!MapReference)
	{
		UE_LOG(LogProtagonistHUD, Warning, TEXT("ToggleMap(): Creating "
			"new map widget as reference was null"));
		CreateMapWidget();
	}
	
	if (MapReference)
	{
		// Only need to toggle if
		// message now is the map
		if (IsMessageTheMap())
		{
			ToggleMessage();
		}
		else
		{
			ShowMessage(MapReference, false, MapMessageSettings);
		}
	}
}

bool AProtagonistHUD::ShowMessage(UPopupWidget* Message, 
	bool bDisplayImmediately, const FMessageSettings& InSettings)
{
	// Don't allow more than one message to be queued
	if (QueuedMessage.Message || !Message)
	{
		return false;
	}

	QueuedMessage.Message = Message;
	QueuedMessage.Settings = InSettings;

	if (bDisplayImmediately)
	{
		HideMessage();
	}

	ToggleMessage();

	return true;
}

void AProtagonistHUD::HideMessage(bool bForceInstantly /*= true*/)
{
	if (IsMessageVisible())
	{
		// If the message was not requested to be hidden
		// instantly, toggle the message so the hide sound can play
		if (bForceInstantly)
		{
			DisplayedMessage.Message->Hide(true);
		}
		else
		{
			ToggleMessage();
		}
	}
}

void AProtagonistHUD::HideAll()
{
	HideMessage();
	SetWidgetVisibility(TipReference, ESlateVisibility::Hidden);
	SetWidgetVisibility(NotifyListReference, ESlateVisibility::Hidden);
}

void AProtagonistHUD::TogglePauseMenu()
{
	if (PauseReference)
	{
		StartPauseDestroy();
	}
	else
	{
		if (CreatePauseWidget())
		{
			StartPauseDisplay();
		}
	}
}

bool AProtagonistHUD::IsMessageWithdrawn() const
{
	UPopupWidget* Message = DisplayedMessage.Message;
	if (Message)
	{
		return Message->IsWithdrawn();
	}

	return false;
}

bool AProtagonistHUD::IsMessageVisible() const
{
	UPopupWidget* Message = DisplayedMessage.Message;
	if (Message)
	{
		return Message->IsWithdrawn() || Message->IsPopping();
	}

	return false;
}

bool AProtagonistHUD::IsMessageTheMap() const
{
	UPopupWidget* const Message = DisplayedMessage.Message;
	return MapReference == Message;
}

void AProtagonistHUD::ToggleInventoryItemIcon(UItemAsset* Item, int32 Index) const
{
	if (MapReference)
	{
		UInventoryWidget* Inventory = MapReference->GetInventoryWidget();
		if (Inventory)
		{
			if (Item)
			{
				Inventory->SetItemIcon(Item, Index);
			}
			else
			{
				Inventory->ClearItemIcon(Index);
			}
		}
		else
		{
			UE_LOG(LogProtagonistHUD, Warning, TEXT("Could not toggle items "
				"icon. \"%s\" inventory widget is null"), *GetName());
		}
	}
}

void AProtagonistHUD::SetProtagonistMarkerPosition(const FVector& Position) const
{
	if (MapReference)
	{
		UMinimapWidget* Minimap = MapReference->GetMinimapWidget();
		if (Minimap)
		{
			Minimap->SetDefaultMarkerPosition(Position);
		}
		else
		{
			UE_LOG(LogProtagonistHUD, Warning, TEXT("Could not set protagonists "
				"minimap position. \"%s\" minimap widget is null"), *GetName());
		}
	}
}

UTexture2D* AProtagonistHUD::GetDefaultCrosshair()
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> DefaultMotion(
		TEXT("/Game/Characters/Player/Textures/T_DefaultCrosshair"));
	return DefaultMotion.Object;
}

void AProtagonistHUD::ToggleMessage()
{
	UPopupWidget* Message = DisplayedMessage.Message;
	if (Message)
	{
		const FMessageSettings& Settings = DisplayedMessage.Settings;

		// Only toggle if no animation is playing
		// unless the toggle is not being stalled
		if (Settings.bInstantlyToggle || (!Message->IsPopping() || !Settings.bStallToggleRequest))
		{
			if (Message->IsWithdrawn())
			{
				StartMessageHide();
			}
			// Only allow the map to be withdrawn again
			else if (IsMessageTheMap())
			{
				StartMessageWithdraw(false);
			}
		}
	}
	else
	{
		// Initialise the queued message if
		// a message is waiting to be displayed
		Message = QueuedMessage.Message;
		if (Message)
		{
			DisplayedMessage = QueuedMessage;
			QueuedMessage.Reset();
			StartMessageWithdraw(true);
		}
	}
}

bool AProtagonistHUD::CreateMapWidget(bool bOverwrite /*= false*/)
{
	if (!MapTemplate)
	{
		UE_LOG(LogProtagonistHUD, Warning, TEXT("No map " 
			"template has been provided for \"%s\""), *GetName());
		return false;
	}

	if (!MapReference)
	{	
		MapReference = CreateWidget<UMapPopupWidget>(PlayerOwner, MapTemplate);
	}
	else if (bOverwrite)
	{
		// Make sure to remove the map from the viewport
		MapReference->RemoveFromParent();
		MapReference = TYPE_OF_NULLPTR();
		MapReference = CreateWidget<UMapPopupWidget>(PlayerOwner, MapTemplate);
	}

	return MapReference != TYPE_OF_NULLPTR();
}

bool AProtagonistHUD::CreatePauseWidget()
{
	if (!PauseTemplate)
	{
		UE_LOG(LogProtagonistHUD, Warning, TEXT("No pause "
			"template has been provided for \"%s\""), *GetName());
		return false;
	}

	// Do not duplicate the pause menu if already created
	if (!PauseReference)
	{
		PauseReference = CreateWidget<UPauseMenuWidget>(PlayerOwner, PauseTemplate);
		if (PauseReference)
		{
			PauseReference->OnFadeOutStart.AddUFunction(this, TEXT("PauseStartedFadingOut"));
			PauseReference->OnFadeOutFinished.AddUFunction(this, TEXT("PauseFinishedFadingOut"));
		}

		return PauseReference != TYPE_OF_NULLPTR();
	}
	else
	{
		return false;
	}
}

void AProtagonistHUD::StartMessageWithdraw(bool bInit)
{
	UPopupWidget* Message = DisplayedMessage.Message;

	// Don't need to add message to viewport
	// if message is already in the viewport
	if (!Message->IsInViewport())
	{
		Message->AddToViewport();
	}

	if (bInit)
	{
		Message->SetOwningPlayer(PlayerOwner);
		DisplayedMessage.Handle = Message->OnHiddenFinished.AddUFunction(this, TEXT("OnMessageHidden"));
	}
	
	const FMessageSettings& Settings = DisplayedMessage.Settings;

	Message->Withdraw(Settings.bInstantlyToggle);
	PlayPopupSoundEffect(Message->WithdrawSound);

	// Temporarily disable subtitles if active
	if (GEngine)
	{
		GEngine->bSubtitlesForcedOff = true;
	}
}

void AProtagonistHUD::StartMessageHide()
{
	UPopupWidget* Message = DisplayedMessage.Message;

	const FMessageSettings& Settings = DisplayedMessage.Settings;

	Message->Hide(Settings.bInstantlyToggle);
	PlayPopupSoundEffect(Message->HideSound);
}

void AProtagonistHUD::StartPauseDisplay()
{
	// Pause the messages animation in-case its playing
	if (DisplayedMessage.Message)
	{
		DisplayedMessage.Message->PausePop();
	}

	PauseReference->AddToViewport();

	// If the player is the local player, re-center their mouse
	UPlayer* Player = PlayerOwner->Player;
	if (Player && PlayerOwner->IsLocalController())
	{
		ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
		FViewport* Viewport = LocalPlayer->ViewportClient->Viewport;

		FVector2D Center;
		LocalPlayer->ViewportClient->GetViewportSize(Center);
		Center /= 2;
		Viewport->SetMouse(static_cast<int32>(Center.X),
			static_cast<int32>(Center.Y));

	}

	PlayerOwner->SetInputMode(FInputModeUIOnly());
	PlayerOwner->SetPause(true);
	PlayerOwner->bShowMouseCursor = true;

	PlayPauseSoundEffect(PauseSound);
}

void AProtagonistHUD::StartPauseDestroy()
{
	if (PauseReference && PauseReference->IsClosing())
	{
		PauseReference->ClosePauseMenu();
	}
}

void AProtagonistHUD::OnMessageHidden()
{
	// Remove any bindings from this message
	UPopupWidget* Message = DisplayedMessage.Message;
	Message->OnHiddenFinished.Remove(DisplayedMessage.Handle);
	
	// Don't remove the map from viewport, as variables
	// might be reset by the native construct event
	if (Message != MapReference)
	{
		Message->RemoveFromParent();
	}

	if (GEngine)
	{
		GEngine->bSubtitlesForcedOff = false;
	}

	// Nullify the current message and display
	// and potential queued message waiting
	DisplayedMessage.Reset();
	ToggleMessage();
}

void AProtagonistHUD::PauseStartedFadingOut()
{
	PlayPauseSoundEffect(UnpauseSound);
}

void AProtagonistHUD::PauseFinishedFadingOut()
{
	// Handle the pause settings if desired
	if (bHandlePauseInputSettings && PlayerOwner)
	{
		PauseReference->RemoveFromParent();
		PauseReference = TYPE_OF_NULLPTR();

		PlayerOwner->SetInputMode(FInputModeGameOnly());
		PlayerOwner->SetPause(false);
		PlayerOwner->bShowMouseCursor = false;
	}

	// Resume playing the pop animation if it was paused
	if (DisplayedMessage.Message)
	{
		DisplayedMessage.Message->ResumePop();
		
		// Resume playing the associated sound for the message
		if (MessageAudio->bIsPaused)
		{
			MessageAudio->SetPaused(false);
		}
	}
}

void AProtagonistHUD::PlayPopupSoundEffect(USoundBase* SoundEffect) const
{
	if (SoundEffect)
	{
		// Make sure to stop the current sound effect
		// to prevent the overlapping of sounds.
		if (MessageAudio->IsPlaying())
		{
			MessageAudio->Stop();
		}

		MessageAudio->SetSound(SoundEffect);
		MessageAudio->Play();
	}
}

void AProtagonistHUD::PlayPauseSoundEffect(USoundBase* SoundEffect) const
{
	// Pause any popup audio thats playing
	if (MessageAudio->IsPlaying())
	{
		MessageAudio->SetPaused(true);
	}

	if (SoundEffect)
	{
		// If a sound is playing right now, just stop it
		if (PauseAudio->IsPlaying())
		{
			PauseAudio->Stop();
		}

		PauseAudio->SetSound(SoundEffect);
		PauseAudio->Play();
	}
}

void AProtagonistHUD::SetWidgetVisibility(UWidget* Widget) const
{
	ESlateVisibility Visibility = bShowHUD ?
		ESlateVisibility::Visible : ESlateVisibility::Hidden;
	SetWidgetVisibility(Widget, Visibility);
}

void AProtagonistHUD::SetWidgetVisibility(UWidget* Widget, ESlateVisibility Visibility) const
{
	if (Widget)
	{
		Widget->SetVisibility(Visibility);
	}
}

void FMessageWrapper::Reset()
{
	Message = TYPE_OF_NULLPTR();
	Handle.Reset();
}