// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AProtagonist;
class AProtagonistController;

#include "ProtagonistCanInteractWith.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UProtagonistCanInteractWith : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * Interface for actors that the player is able to interact with. In order,
 * for the player to be able to interact with the actor, the CanFocusOn
 * interface must also be implemented.
 */
class PURITYGAME_API IProtagonistCanInteractWith
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	/** Called when the protagonist wishes to interact with this object */
	/** @param1: The controller for the protagonist */
	UFUNCTION(BlueprintNativeEvent, Category = "Interaction")
	void OnProtagonistInteract(AProtagonistController* Player);
};

