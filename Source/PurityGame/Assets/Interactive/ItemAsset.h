// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "ItemAsset.generated.h"

/**
 * An asset for items, provides details for the items
 * name and visuals for both world and HUD. 
 *
 * To make a new item, right click in contents, go to the 
 * miscellaneous, select DataAsset and choose ItemAsset.
 */
UCLASS(BlueprintType)
class PURITYGAME_API UItemAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	/** Returns the name of this item */
	FORCEINLINE FText GetItemName() const { return ItemName; }

	/** Returns the icon of this item */
	FORCEINLINE UTexture2D* GetItemIcon() const { return ItemIcon; }

	/** Returns the mesh of this item */
	FORCEINLINE UStaticMesh* GetItemMesh() const { return ItemMesh; }

	/** Returns if this item is a key item */
	FORCEINLINE bool IsKeyItem() const { return bIsKeyItem; }

private:
	
	/** The name of this item */
	UPROPERTY(Category = Item, EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Name"))
	FText ItemName;

	/** The icon of this item */
	UPROPERTY(Category = Item, EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Icon"))
	UTexture2D* ItemIcon;

	/** The mesh of this item */
	UPROPERTY(Category = Item, EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Mesh"))
	UStaticMesh* ItemMesh;

	/** If this item is considered a key item */
	UPROPERTY(Category = Item, EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	uint32 bIsKeyItem : 1;
};
