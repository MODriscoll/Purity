// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Props/Doors/HingedDoorActor.h"
#include "HingedDoubleDoorsActor.generated.h"

/**
 * Door that supports the original hinged door
 * along side another door on the oppisite side.
 */
UCLASS()
class PURITYGAME_API AHingedDoubleDoorsActor : public AHingedDoorActor
{
	GENERATED_BODY()
	
public:

	/** Sets default values */
	AHingedDoubleDoorsActor();

protected:

	/** Swings the door around based on the alpha value */
	virtual void TickDoorMotion(float Alpha) override;

public:

	/** The "hinge" that will rotate in the 
	opposite direction of the initial hinge */
	UPROPERTY(BlueprintReadWrite, Category = Door)
	USceneComponent* OppositeHinge;
};
