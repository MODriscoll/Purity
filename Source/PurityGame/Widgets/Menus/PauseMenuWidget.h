// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UWidgetAnimation;

#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

/** Event for when the pop animation has finished playing */
DECLARE_EVENT(FPauseMenuWidget, FFadedSignature);

/**
 * Widget to be used as a pause menu. Supports a
 * fade in and out animation that will be called
 * when initially created and when requested to be
 * closed.
 */
UCLASS(abstract, meta = (DisplayName = "Pause Menu HUD"))
class PURITYGAME_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

	/** Called when a widget animation has finished playing */
	virtual void OnAnimationFinished_Implementation(
		const UWidgetAnimation* Animation) override;

protected:

	/** Called after the underlaying slate is constructed */
	virtual void NativeConstruct() override;

public:

	/** Returns if the pause menu is currently fading */
	UFUNCTION(BlueprintPure, Category = "Pause HUD")
	bool IsFading() const;

	/** If the pause menu is currently closing */
	UFUNCTION(BlueprintPure, Category = "Pause HUD")
	bool IsClosing() const { return bIsClosing; }
	
	/** Closes the pause menu, can be bound to by a button if desired */
	UFUNCTION(BlueprintCallable, Category = "Pause HUD")
	void ClosePauseMenu();

protected:

	/** Gets the fade animation to play on construct and close */
	UFUNCTION(BlueprintImplementableEvent, Category = "Pause HUD")
	UWidgetAnimation* GetFadeAnimation() const;

	/** Called when the buttons should be enabled/disabled */
	UFUNCTION(BlueprintImplementableEvent, Category = "Pause HUD")
	void EnableButtons(bool bEnable);

public:

	/** Delegate for when the fade animation
	has finished when fading in. */
	FFadedSignature OnFadeOutStart;

	/** Delegate for when the fade animation
	has finished when fading out. */
	FFadedSignature OnFadeOutFinished;

private:

	/** If the menu is opening or closing. Used during animations */
	uint32 bIsClosing : 1;
};
