// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MessagePopupWidget.h"

#include "Components/Image.h"

void UMessagePopupWidget::SetMessage(UTexture2D* Texture)
{
	UImage* MessageImage = GetMessageImage();
	if (MessageImage)
	{
		if (Texture)
		{
			FSlateBrush IconBrush = MessageImage->Brush;
			IconBrush.DrawAs = ESlateBrushDrawType::Image;
			IconBrush.SetResourceObject(Texture);
			MessageImage->SetBrush(IconBrush);
		}
	}
}
