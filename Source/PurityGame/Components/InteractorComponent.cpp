// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "InteractorComponent.h"

#include "Characters/Player/Protagonist.h"
#include "Characters/Player/ProtagonistController.h"

DEFINE_LOG_CATEGORY(LogInteractorComponent);

UInteractorComponent::UInteractorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	InteractDistance = 500.f;
	bTraceComplex = false;
	bUsePawnControlRotation = false;
	bKeepFocusInCinematicMode = false;

	FoundInteractive = TYPE_OF_NULLPTR();
}

void UInteractorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UWorld* World = GetWorld();
	AProtagonistController* Controller = GetInteractingController();

	// Allow check for interactives if there is a world
	// to check in and we have a valid controller to send
	if (World && Controller)
	{
		if (Controller->bCinematicMode && !bKeepFocusInCinematicMode)
		{
			// Only resolve invalid interaction once when cinematic mode is active
			if (FoundInteractive)
			{
				ResolveInvalidInteractive(Controller);
			}

			return;
		}

		// Perform a raycast to check for interactive objects
		FVector Origin, Destination;
		FHitResult Hit(ForceInit);
		FCollisionQueryParams Params(TEXT("InteractorTrace"), true, GetOwner());
		GetTraceParameters(Origin, Destination, Params, Controller);

		bool HasHit = World->LineTraceSingleByChannel(Hit, Origin, Destination, ECC_Visibility, Params);

		// Resolve the outcome of the trace if hit actor is not found interactive and can still be focused
		if (HasHit)
		{
			if (FoundInteractive != Hit.GetActor())
			{
				ResolveTrace(Hit, Controller);
			}
			else if (FoundInteractive && !IProtagonistCanFocusOn::Execute_CanBeFocused(FoundInteractive))
			{
				ResolveInvalidInteractive(Controller);
			}
		}
		else
		{
			ResolveInvalidInteractive(Controller);
		}
	}
}

AActor* UInteractorComponent::GetFoundInteractive() const
{
	// Only return the focused interactive if it can still be focused
	if (FoundInteractive && IProtagonistCanFocusOn::Execute_CanBeFocused(FoundInteractive))
	{
		return FoundInteractive;
	}

	return TYPE_OF_NULLPTR();
}

void UInteractorComponent::GetTraceParameters(FVector& OutOrigin, FVector& OutDestination, 
	FCollisionQueryParams& OutParameters, AProtagonistController* Controller)
{
	// Calculate finishing point of the cast
	FRotator Direction;
	if (bUsePawnControlRotation)
	{
		FRotator ControlRotation = Controller->GetControlRotation();
		Direction = ControlRotation;
		SetWorldRotation(ControlRotation);
	}
	else
	{
		Direction = GetComponentRotation();
	}
	OutOrigin = GetComponentLocation();
	OutDestination = OutOrigin + (Direction.Vector() * InteractDistance);

	// Set parameters for the trace
	OutParameters.bTraceComplex = bTraceComplex;
	OutParameters.bReturnPhysicalMaterial = false;
}

void UInteractorComponent::ResolveTrace(const FHitResult& TraceResult, AProtagonistController* Controller)
{
	// Is hit object an interactable
	AActor* ActorHit = TraceResult.GetActor();

	// Resolve conflict if new interactive object was found
	if (ActorHit->GetClass()->ImplementsInterface(UProtagonistCanFocusOn::StaticClass()) 
		&& IProtagonistCanFocusOn::Execute_CanBeFocused(ActorHit))
	{
		ResolveConflict(ActorHit, Controller);
	}
	else
	{
		ResolveInvalidInteractive(Controller);
	}
}

void UInteractorComponent::ResolveConflict(AActor* AsActor, AProtagonistController* Controller)
{
	// Switch the found interactive object
	ResolveInvalidInteractive(Controller);
	FoundInteractive = AsActor;

	// Call the on focus event only if owner is a protagonist
	IProtagonistCanFocusOn::Execute_OnProtagonistFocus(AsActor, Controller);
	OnObjectFocused.Broadcast(FoundInteractive);
}

void UInteractorComponent::ResolveInvalidInteractive(AProtagonistController* Controller)
{
	if (FoundInteractive)
	{
		IProtagonistCanFocusOn::Execute_OnProtagonistUnfocus(FoundInteractive, Controller);
		OnObjectUnfocused.Broadcast(FoundInteractive);

		// Clear the old interactive object
		FoundInteractive = TYPE_OF_NULLPTR();
	}
}

AProtagonistController* UInteractorComponent::GetInteractingController() const
{
	AProtagonistController* Controller = TYPE_OF_NULLPTR();

	AActor* Owner = GetOwner();

	// First check if owner of this component is a protagonist,
	// if so, cast its controller to the protagonist controller
	{
		AProtagonist* AsProtagonist = Cast<AProtagonist>(Owner);
		if (AsProtagonist)
		{
			Controller = Cast<AProtagonistController>(AsProtagonist->GetController());
		}
	}

	// If controller wasn't found, check if the owner
	// is the protagonist controller thats interacting
	if (!Controller)
	{
		Controller = Cast<AProtagonistController>(Owner);
	}

	return Controller;
}

