// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "DoorActor.h"

DEFINE_LOG_CATEGORY(LogDoorActor);

ADoorActor::ADoorActor()
{
	// Set default values
	bIsCosmetic = false;
	bStartOpen = false;
	bAutoClose = false;
	bCutSwingSounds = true;

	AutoCloseAfter = 5.f;

	MotionCurve = GetDefaultMotion();

	// Create the audio component and set it as the root of this actor
	AudioSource = CreateDefaultSubobject<UAudioComponent>(TEXT("Swining Sounds"));
	AudioSource->SetupAttachment(RootComponent);

	// Set default settings for the audio
	AudioSource->bIsUISound = false;
	AudioSource->bIsMusic = false;
	AudioSource->bAutoDestroy = false;
	AudioSource->bSuppressSubtitles = true;

	// Create the timeline component and attach it to this actor
	MotionTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("Motion Timeline"));
	AddOwnedComponent(MotionTimeline);
}

void ADoorActor::BeginPlay()
{
	Super::BeginPlay();

	// Initialise the timeline for the door
	{
		// If no curve was set, use the default curve
		UCurveFloat* OpenCurve = MotionCurve ? MotionCurve : GetDefaultMotion();

		// Create the need events
		FOnTimelineFloat UpdateFunc{};
		UpdateFunc.BindDynamic(this, &ADoorActor::TickDoorMotion);

		FOnTimelineEvent FinishFunc{};
		FinishFunc.BindDynamic(this, &ADoorActor::OnMotionFinish);

		// Attach the curve and events to the timeline
		MotionTimeline->AddInterpFloat(OpenCurve, UpdateFunc, FName(TEXT("Swing Time")));
		MotionTimeline->SetTimelineFinishedFunc(FinishFunc);
	}

	// If not cosmetic, unrotate any rotation that might be set
	if (!bIsCosmetic)
	{
		TickDoorMotion(bStartOpen ? 1.f : 0.f);
		bIsOpen = bStartOpen;
	}
}

void ADoorActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	FTimerManager& Manager = GetWorldTimerManager();
	if (Manager.IsTimerActive(AutoCloseHandle))
	{
		Manager.ClearTimer(AutoCloseHandle);
	}
}

void ADoorActor::Open(const AActor* OpenedBy)
{
	if (!bIsCosmetic && !IsSwinging()
		&& !bIsOpen)
	{
		StartOpenDoor(OpenedBy);
	}
}

void ADoorActor::Close(const AActor* ClosedBy)
{
	if (!bIsCosmetic && !IsSwinging()
		&& bIsOpen)
	{
		StartCloseDoor(ClosedBy);
	}
}

bool ADoorActor::IsOpen() const
{
	return bIsOpen;
}

bool ADoorActor::IsSwinging() const
{
	return MotionTimeline->IsPlaying() || MotionTimeline->IsReversing();
}

void ADoorActor::SetAutoClose(bool bAuto)
{
	bAutoClose = bAuto;

	if (bAutoClose)
	{
		// Set timer now if door is open and the timer handle has not been set
		bool bIsFullyOpen = bIsOpen && !MotionTimeline->IsPlaying();
		if (bIsFullyOpen && !AutoCloseHandle.IsValid())
		{
			FTimerManager& Manager = GetWorldTimerManager();
			FTimerDelegate Delegate;
			Delegate.BindUFunction(this, FName("Close"), this);
			Manager.SetTimer(AutoCloseHandle, Delegate, AutoCloseAfter, false);
		}
	}
}

void ADoorActor::OnMotionFinish()
{
	// Call event based on if door is open or not
	if (bIsOpen)
	{
		// Set auto close timer if auto close is enabled
		if (bAutoClose)
		{
			FTimerManager& Manager = GetWorldTimerManager();
			FTimerDelegate Delegate;
			Delegate.BindUFunction(this, FName("Close"), this);
			Manager.SetTimer(AutoCloseHandle, Delegate, AutoCloseAfter, false);
		}

		OnDoorOpened.Broadcast();
		PlayDoorSound(OpeningSounds.SwingFinishSound);
	}
	else
	{
		OnDoorClosed.Broadcast();
		PlayDoorSound(ClosingSounds.SwingFinishSound);
	}
}

void ADoorActor::StartOpenDoor(const AActor* OpenedBy)
{
	MotionTimeline->PlayFromStart();

	// Execute the on door open start event
	bIsOpen = true;
	OnDoorOpening.Broadcast();

	PlayDoorSound(OpeningSounds.SwingStartSound);
}

void ADoorActor::StartCloseDoor(const AActor* ClosedBy)
{
	MotionTimeline->ReverseFromEnd();

	// Invalidate the auto close timer
	FTimerManager& Manager = GetWorldTimerManager();
	if (Manager.IsTimerActive(AutoCloseHandle))
	{
		Manager.ClearTimer(AutoCloseHandle);
	}

	// Execute the on door close start event
	bIsOpen = false;
	OnDoorClosing.Broadcast();

	PlayDoorSound(ClosingSounds.SwingStartSound);
}

void ADoorActor::PlayDoorSound(USoundBase* Sound)
{ 
	bool bPlaySound = bCutSwingSounds;

	// The new sound will always play
	// if we are cutting sounds
	if (bCutSwingSounds)
	{
		if (AudioSource->IsPlaying())
		{
			AudioSource->Stop();
		}
	}
	else
	{
		// If a sound is playing, let it finish
		bPlaySound = !AudioSource->IsPlaying();
	}

	if (bPlaySound)
	{
		AudioSource->SetSound(Sound);
		AudioSource->Play();
	}
}

UCurveFloat* ADoorActor::GetDefaultMotion()
{
	static ConstructorHelpers::FObjectFinder<UCurveFloat> DefaultMotion(
		TEXT("/Game/Base/Curves/FloatCurves/CU_DefaultDoorMotion"));
	return DefaultMotion.Object;
}


