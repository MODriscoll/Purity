// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "PopupWidget.generated.h"

/** Event for when the pop animation has finished playing */
DECLARE_EVENT(FPopupWidget, FPoppedSignature);

DECLARE_LOG_CATEGORY_EXTERN(LogPopupWidget, Log, All);

/**
 * Widget that supports playing an animation to be
 * used for popping the widget either on-screen or
 * of screen. Is designed to work with the protagonist
 * HUD. It does not need to be utilised like
 * this though. If overriding the animation finished 
 * function, you must call parent function.
 */
UCLASS()
class PURITYGAME_API UPopupWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

	/** Called when a widget animation has finished playing */
	virtual void OnAnimationFinished_Implementation(
		const UWidgetAnimation* Animation) override;

protected:

	/** Called after underlaying slate is constructed */
	virtual void NativeConstruct() override;

public:

	/** Plays the popup animation. Is
	expected to move the HUD on screen */
	UFUNCTION(BlueprintCallable, Category = Popup)
	void Withdraw(bool bInstantly = false, bool bForce = false);

	/** Plays the popup animation in reverse.
	Is expected to move the HUD off screen */
	UFUNCTION(BlueprintCallable, Category = Popup)
	void Hide(bool bInstantly = false, bool bForce = false);

	/** Pauses the popup animation if currently playing */
	UFUNCTION(BlueprintCallable, Category = Popup)
	void PausePop();

	/** Resumes the popup animation if was playing */
	UFUNCTION(BlueprintCallable, Category = Popup)
	void ResumePop();

	/** Get if this popup has been withdrawn */
	bool IsWithdrawn() const { return bIsWithdrawn; }

	/** Get if this popup is in the 
	process of being withdrawn/hidden */
	UFUNCTION(BlueprintPure, Category = Popup)
	bool IsPopping() const;
	
protected:

	/** Get the animation to play when popping */
	UFUNCTION(BlueprintImplementableEvent, Category = Popup)
	UWidgetAnimation* GetPopAnimation() const;

private:

	/** Plays the pop animation either forward or reversed */
	bool PlayPopAnimation(EUMGSequencePlayMode::Type PlayMode, bool bInstantly);

public:

	/** Delegate for when the pop animation
	finished playing when being withdrawn */
	FPoppedSignature OnWithdrawnFinished;

	/** Delegate for when the pop animation
	finished playing when being hidden */
	FPoppedSignature OnHiddenFinished;

	/** The sound for the HUD to play when withdrawing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup)
	USoundBase* WithdrawSound;

	/** The sound for the HUD to play when hiding */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup)
	USoundBase* HideSound;

private:

	/** If the map is assumed to be onscreen */
	UPROPERTY(BlueprintReadOnly, Category = Popup,
		meta = (AllowPrivateAccess = "true"))
	uint32 bIsWithdrawn : 1;

	/** If the finished events should be ignored. Is
	set when either withdraw or hide is called while
	the pop animation is currently playing */
	UPROPERTY(Transient)
	uint32 bSkipOnFinished : 1;

	/** The time of the animation if paused */
	float PausedTime;
};
