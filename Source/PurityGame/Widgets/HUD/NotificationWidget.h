// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UImage;
class UVerticalBox;

#include "Blueprint/UserWidget.h"
#include "NotificationWidget.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogNotificationWidget, Log, All);

UCLASS(abstract, meta = (DisplayName = "Notification"))
class PURITYGAME_API UNotificationWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

protected:

	/** Get the symbol for this notification */
	//UFUNCTION(BlueprintImplementableEvent, Category = Notification)
	//UImage* GetNotificationSymbol() const;

public:

	/** The notification to display to the user */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Notification)
	FText Notification;

	/** The symbol to highlight this notification */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Notification)
	UTexture2D* Symbol;
};

UENUM()
enum class ENotificationState : uint8
{
	Active,
	Fading,
	Expired
};

USTRUCT()
struct PURITYGAME_API FNotificationHandle
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FNotificationHandle()
	{
		Notification = TYPE_OF_NULLPTR();
		LifeSpan = 1.f;
		FadeOutSpan = 1.f;
		State = ENotificationState::Expired;
		StateTime = 0.f;
	}

	/** Initializes values */
	FNotificationHandle(UNotificationWidget* InNotification, 
		float InLifeSpan, float InFadeOutSpan)
	{
		Notification = InNotification;

		LifeSpan = InLifeSpan > 0.f ? InLifeSpan : 2.f;
		FadeOutSpan = InFadeOutSpan > 0.f ? InFadeOutSpan : 1.f;

		State = ENotificationState::Active;
		StateTime = 0.f;
	}

public:

	/** Updates this handle, returns the
	opacity to draw the widget with */
	float Update(float DeltaTime);

	/** Get the total expected life span of this notification */
	float GetTotalSpan() const { return LifeSpan + FadeOutSpan; }

	/** Get the state of this handle */
	ENotificationState GetState() const { return State; }

	/** Get if this state has expired */
	bool HasExpired() const { return State == ENotificationState::Expired; }

public:

	/** The notification being handled */
	UPROPERTY()
	UNotificationWidget* Notification;

	/** The life span for this widget */
	UPROPERTY()
	float LifeSpan;

	/** The fade out span for this widget */
	UPROPERTY()
	float FadeOutSpan;

private:

	/** The state this notification is in */
	UPROPERTY()
	ENotificationState State;

	/** The time this the current state has been active for */
	UPROPERTY()
	float StateTime;
};

UCLASS(abstract, meta = (DisplayName = "Notification List"))
class PURITYGAME_API UNotificationListWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/** Sets default values */
	UNotificationListWidget(const FObjectInitializer& ObjectInitializer);

public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

protected:

	/** Called when the underlaying slate is constructed */
	virtual void NativeConstruct() override;

	/** Called when the underlaying slate is destroyed */
	virtual void NativeDestruct() override;

	/** Called every frame */
	virtual void NativeTick(const FGeometry& InGeometry, float InDeltaTime) override;

public:

	/** Returns the amount of notifications currently
	displayed. This includes notifications that are fading out */
	UFUNCTION(BlueprintPure, Category = Notification)
	int32 GetNotificationCount(bool bIncludeFading = true) const;

	/** Pushes a new notification to the list */
	UFUNCTION(BlueprintCallable, Category = Notification)
	UNotificationWidget* PushNotification(float InLifeSpan,
		TSubclassOf<UNotificationWidget> Template = nullptr);

protected:

	/** Get the vertical box to add notifications to */
	UFUNCTION(BlueprintImplementableEvent, Category = Notification)
	UVerticalBox* GetVerticalList() const;

public:

	/** The default template to use when creating notifications */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Notification)
	TSubclassOf<UNotificationWidget> NotificationTemplate;

	/** The time to fade out notifications */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Notification)
	float FadeOutTime;

private:

	/** All the notifications currently in the list */
	UPROPERTY(Transient)
	TArray<FNotificationHandle> Notifications;
};