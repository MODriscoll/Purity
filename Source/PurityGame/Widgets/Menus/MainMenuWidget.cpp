// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MainMenuWidget.h"

#include "GameModes/Menu/MainMenuGameModeBase.h"
#include "Characters/Player/MenuHUD.h"

#include "Kismet/GameplayStatics.h"

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UMainMenuWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}

#endif

void UMainMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	// We are only concerned for the pop animation.
	// Don't call the events on other animations
	if (Animation == GetFadeAnimation())
	{
		if (LevelToOpenName != NAME_None)
		{
			UGameplayStatics::OpenLevel(GetWorld(), LevelToOpenName);
			LevelToOpenName = NAME_None;
		}
		else
		{
			EnableButtons(true);
		}
	}
}

void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	LevelToOpenName = NAME_None;
	bIsLevelLoading = false;

	// Play the initial fade in sequence
	UWidgetAnimation* FadeAnimation = GetFadeAnimation();
	if (FadeAnimation)
	{
		PlayAnimation(FadeAnimation, 0.f, 1, EUMGSequencePlayMode::Forward);
	}

	EnableButtons(!FadeAnimation);
}

void UMainMenuWidget::NativeDestruct()
{
	Super::NativeDestruct();
	
	SetMainMenu(TYPE_OF_NULLPTR());
}

void UMainMenuWidget::SetMainMenu(AMainMenuGameModeBase* NewMainMenu)
{
	// Make sure no level is loading as of now
	// No need to update if menus are the same
	if (NewMainMenu != MainMenu && !bIsLevelLoading)
	{
		// Remove the callbacks from the current menu
		if (MainMenu)
		{
			MainMenu->OnLoadSuccessfull.RemoveDynamic(this, &UMainMenuWidget::OnLoadSuccess);
			MainMenu->OnLoadFailed.RemoveDynamic(this, &UMainMenuWidget::OnLoadFail);
		}

		// Make sure new main menu is valid
		if (NewMainMenu)
		{
			NewMainMenu->OnLoadSuccessfull.AddDynamic(this, &UMainMenuWidget::OnLoadSuccess);
			NewMainMenu->OnLoadFailed.AddDynamic(this, &UMainMenuWidget::OnLoadFail);
		}

		MainMenu = NewMainMenu;
	}
}

void UMainMenuWidget::SetMenuHUD(AMenuHUD* NewMenuHUD)
{
	if (MenuHUD)
	{
		MenuHUD->ClearSubMenu();
	}

	MenuHUD = NewMenuHUD;
}

void UMainMenuWidget::FadeOutToNewLevel(FName LevelName)
{
	if (MainMenu && MainMenu->LoadLevel(LevelName.ToString()))
	{
		bIsLevelLoading = true;
		EnableButtons(false);
	}
}

USubMenuWidget* UMainMenuWidget::OpenSubMenu(TSubclassOf<USubMenuWidget> Template, float Alpha)
{
	if (MenuHUD)
	{
		return MenuHUD->CreateSubMenu(Template, Alpha);
	}

	return TYPE_OF_NULLPTR();
}

void UMainMenuWidget::OnLoadSuccess(const FName& LevelName)
{
	LevelToOpenName = LevelName;

	// If no fade animation is provided,
	// skip straight to opening the level
	UWidgetAnimation* FadeAnimation = GetFadeAnimation();
	if (FadeAnimation)
	{
		PlayAnimation(FadeAnimation, 0.f, 1, EUMGSequencePlayMode::Reverse);
	}
	else
	{
		UGameplayStatics::OpenLevel(GetWorld(), LevelToOpenName);
		LevelToOpenName = NAME_None;
	}
}

void UMainMenuWidget::OnLoadFail(const FName& LevelName)
{
	// Allow for interaction with the menu again
	bIsLevelLoading = false;
	EnableButtons(true);

	LevelToOpenName = NAME_None;
}

#undef LOCTEXT_NAMESPACE