// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AElevatorActor;
class AElevatorButtonActor;
class ADoorActor;

#include "GameFramework/Actor.h"
#include "ElevatorActor.generated.h"

UENUM(BlueprintType)
enum class EElevatorState : uint8
{
	/** The elevator is waiting for the doors to close */
	PreTransition,

	/** The elevator is elevating to the requested floor */
	Transitioning,

	/** The elevator is waiting for the doors to open */
	PostTransition,

	/** The elevator has finished its transition and is now waiting */
	Waiting
};

/** Delegate for when the elevator has transitioned to a new phase */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FElevatorStateChangeSignature, EElevatorState, NewState);

DECLARE_LOG_CATEGORY_EXTERN(LogElevatorActor, Log, All);

/**
* A simple elevator that physically moves up and down in
* the world. The floors can be set using vectors where only
* the Z co-ordinate is utilised.
*/
UCLASS()
class PURITYGAME_API AElevatorActor : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Sets default values for this actor's properties */
	AElevatorActor();

public:

	/** Called when actor is spawned or placed in editor */
	virtual void OnConstruction(const FTransform& Transform) override;

	/** Called every frame */
	virtual void Tick(float DeltaSeconds) override;

protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

	/** Called when the game ends or destroyed */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	/** Set the floor to goto if able to */
	UFUNCTION(BlueprintCallable, Category = Elevator)
	bool GotoFloor(int32 FloorIndex);

	/** Get if this elevator is currently transitioning */
	UFUNCTION(BlueprintCallable, Category = Elevator)
	bool IsTransitioning() const { return TransitionState != EElevatorState::Waiting; }

	/** Get the base mesh of the elevator */
	UStaticMeshComponent* GetBaseMesh() const { return BaseMesh; }

	/** Gets the index of the floor the elevator is at. If elevator
	is transitioning, returns floor elevator is escalating to */
	int32 GetFloorIndex() const { return TransitionToIndex; }

	/** Gets the index of the floor the elevator was previously at */
	int32 GetPreviousFloorIndex() const { return TransitionFromIndex; }

	/** Returns the estimated level the elevator is at
	@param1: The estimated floor index of the elevator
	@result: If the elevator is transitioning */
	UFUNCTION(BlueprintCallable, Category = Elevator)
	bool GetEstimatedFloorIndex(int32& OutEstimatedIndex) const;

	/** Get the floors of the elevator */
	const TArray<FVector>& GetFloors() const { return FloorLevels; }

private:

	/** Converts the floor vectors from local space to world space */
	void CalculateWorldFloorLocations();

	/** Starts the transition process to the elevator */
	void StartTransition(int32 FloorIndex);

	/** Starts the elevation process to the next floor */
	UFUNCTION()
	void StartElevation();

	/** Finishes the transition process of the elevator */
	UFUNCTION()
	void FinishTransition();

	/** Finishes the elevation process to the next floor */
	void FinishElevation();

	/** Moves the elevator towards the set floor,
	returns if elevator has arrived at the floor */
	bool Elevate(float DeltaSeconds);

	/** Changes the state of this elevator */
	void SetState(EElevatorState NewState);

	/** Called when one of the elevator buttons are pressed */
	UFUNCTION()
	void PanelButtonPressed(AButtonActor* Button, AActor* PressedBy);

	/** Enables/disables all of the buttons */
	void ToggleButtons(bool bEnable);

public:

	/** Delegate for when the elevator enters a new state
		@param1: The state the elevator has entered */
	UPROPERTY(BlueprintAssignable, Category = Elevator)
	FElevatorStateChangeSignature OnElevatorStateChanged;

	/** The speed of the elevator */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Elevator)
	float Speed;

	/** The floor at which to start at */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Elevator, meta = (ClampMin = "0"))
	int32 StartAtFloor;

private:

	/** The mesh to represent the elevators base */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BaseMesh;

		/** The state this elevator is in */
	UPROPERTY(BlueprintReadOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	EElevatorState TransitionState;

	/** The floor the elevator is transitioning from */
	UPROPERTY(BlueprintReadOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	int32 TransitionFromIndex;

	/** The floor the elevator is transitioning to */
	UPROPERTY(BlueprintReadOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	int32 TransitionToIndex;

	/** The door of the elevator */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	ADoorActor* ElevatorDoor;

	/** All the buttons associated with this elevator */
	UPROPERTY(EditInstanceOnly, Category = Elevator, meta = (AllowPrivateAccess = "true"))
	TArray<AElevatorButtonActor*> ElevatorButtons;

	/** The levels that this elevator can travel to */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Elevator, 
		meta = (MakeEditWidget = "true", AllowPrivateAccess = "true", DisplayName = "Floors"))
	TArray<FVector> FloorLevels;
};
