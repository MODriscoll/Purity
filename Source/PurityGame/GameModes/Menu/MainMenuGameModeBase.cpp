// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MainMenuGameModeBase.h"

#include "Characters/Player/MenuPlayerController.h"
#include "Characters/Player/MenuHUD.h"

#include "Widgets/HUD/LoadingWidget.h"

#include "Engine/StreamableManager.h"

#define LOCTEXT_NAMESPACE "Loading"

AMainMenuGameModeBase::AMainMenuGameModeBase()
{
	PlayerControllerClass = AMenuPlayerController::StaticClass();
	HUDClass = AMenuHUD::StaticClass();
	bPauseable = false;
	bStartPlayersAsSpectators = false;
}

bool AMainMenuGameModeBase::LoadLevel(const FString& PackageName)
{
	FString PackagePath = PackageName;
	if (GEngine->MakeSureMapNameIsValid(PackagePath))
	{ 
		// Make sure nothing is already loading
		if (PackageReference.IsNull() ||
			PackageStreamer.IsAsyncLoadComplete(PackageReference))
		{
			PackageReference.SetPath(PackagePath);

			// Remove the loading widget if it exists as the
			// template could have been changed since last time
			if (LoadWidgetReference)
			{
				LoadWidgetReference->RemoveFromParent();
			}

			if (LoadTemplate)
			{
				// Attach the widget to the local player
				APlayerController* LocalPlayer = TYPE_OF_NULLPTR();
				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
				{
					APlayerController* Player = It->Get();
					if (Player->IsLocalPlayerController())
					{
						LocalPlayer = Player;
					}
				}

				LoadWidgetReference = CreateWidget<ULoadingWidget>(LocalPlayer, LoadTemplate);
				LoadWidgetReference->AddToViewport();
			}

			FStreamableDelegate Delegate;
			Delegate.BindUFunction(this, TEXT("OnLevelFinishedLoading"));
			PackageStreamer.RequestAsyncLoad(PackageReference, Delegate);

			return true;
		}
	}

	return false;
}

void AMainMenuGameModeBase::UnloadLevel()
{
	if (PackageReference.IsValid())
	{
		PackageStreamer.Unload(PackageReference);
	}
}

void AMainMenuGameModeBase::OnLevelFinishedLoading()
{
	if (LoadWidgetReference)
	{
		LoadWidgetReference->RemoveFromParent();
	}

	// Shorten the full package to the asset name
	FString LevelAssetName = FPaths::GetBaseFilename(PackageReference.ToString());
	FName LevelName = FName(*LevelAssetName);

	if (PackageReference.IsValid())
	{
		OnLoadSuccessfull.Broadcast(LevelName);
	}
	else
	{
		OnLoadFailed.Broadcast(LevelName);
	}
}

#undef LOCTEXT_NAMESPACE