// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DoorActor.generated.h"

USTRUCT(BlueprintType)
struct PURITYGAME_API FDoorSwingSoundData
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FDoorSwingSoundData()
	{
		SwingStartSound = TYPE_OF_NULLPTR();
		SwingFinishSound = TYPE_OF_NULLPTR();
	}

public:

	/** The sound for when the door starts swinging */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sounds)
	USoundBase* SwingStartSound;

	/** The sound for when the door starts swinging */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sounds)
	USoundBase* SwingFinishSound;
};

DECLARE_LOG_CATEGORY_EXTERN(LogDoorActor, Log, All);

/** Delegate for when the door has finished swinging open or closed */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorMotionSignature);

/**
 * Base class for doors, uses a curve and timelines
 * to open and close the door with events attached.
 * This actor should not be placed in the world, but
 * should inherited from.
 */
UCLASS(abstract)
class PURITYGAME_API ADoorActor : public AActor
{
	GENERATED_BODY()
	
public:

	/** Sets the default values */
	ADoorActor();

protected:

	/** Called when the game starts */
	virtual void BeginPlay() override;

	/** Called when the game ends */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	/** Opens the door */
	UFUNCTION(BlueprintCallable, Category = Interactive, meta = (Keywords = "Open Door"))
	void Open(const AActor* OpenedBy);

	/** Closes the door */
	UFUNCTION(BlueprintCallable, Category = Interactive, meta = (Keywords = "Close Door"))
	void Close(const AActor* ClosedBy);

	/** Returns if the door is currently open */
	bool IsOpen() const;

	/** Returns if the door is swinging either open or close */
	UFUNCTION(BlueprintCallable, Category = Door)
	bool IsSwinging() const;

	/* Set if this door should automatically close or not */
	UFUNCTION(BlueprintCallable, Category = Door, meta = (DisplayName = "Set Automatically Close", Keywords = "Close Door"))
	void SetAutoClose(bool bAuto);

	/** Returns if this door will automatically close */
	bool WillAutoClose() const { return bAutoClose; }

	/** Get the default motion for all doors */
	static UCurveFloat* GetDefaultMotion();

protected:

	/** Prepares door to be opened based on it's current state */
	virtual void StartOpenDoor(const AActor* OpenedBy);

	/** Prepares door to be closed based on it's current state */
	virtual void StartCloseDoor(const AActor* ClosedBy);

	/** Plays the current sound from the audio source */
	void PlayDoorSound(USoundBase* Sound);

	/** Set the position of the doors based on the alpha from the timeline */
	UFUNCTION()
	virtual void TickDoorMotion(float Alpha) { }

	/** Called when the timeline ends */
	UFUNCTION()
	virtual void OnMotionFinish();

public:

	/** Delegate for when the door has started opening */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorMotionSignature OnDoorOpening;

	/** Delegate for when the door has finished opening */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorMotionSignature OnDoorOpened;

	/** Delegate for when the door has started closing */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorMotionSignature OnDoorClosing;

	/** Delegate for when the door has finished closing */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorMotionSignature OnDoorClosed;

	/** If this door is just for cosmetic use */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Interp, Category = Door)
	uint32 bIsCosmetic : 1;

	/** If this door should start open when reset */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door)
	uint32 bStartOpen : 1;

	/** If this door is set to automatically close, after what time 
	(in seconds) from finishing opening should the door close itself */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door, meta = (DisplayName = "Automatically Close After"))
	float AutoCloseAfter;

	/** The sound data for when this door is swinging open */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door|Sound")
	FDoorSwingSoundData OpeningSounds;

	/** The sound data for when this door is swinging closed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door|Sound")
	FDoorSwingSoundData ClosingSounds;

	/** If the door swing sounds should cut to allow the next sound to play */
	UPROPERTY(Interp, BlueprintReadWrite, Category = "Door|Sound")
	uint32 bCutSwingSounds : 1;

private:

	/** If the door is currently open */
	UPROPERTY(Category = Door, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	uint32 bIsOpen : 1;

	/** If this door should automatically close after being opened */
	UPROPERTY(Category = Door, EditAnywhere, BlueprintReadOnly, 
		meta = (AllowPrivateAccess = "true", DisplayName = "Automatically Close"))
	uint32 bAutoClose : 1;

	/** The timeline for the doors motion, this is used to open and close the door */
	UPROPERTY(Category = Door, BlueprintReadOnly, 
		meta = (AllowPrivateAccess = "true"))
	UTimelineComponent* MotionTimeline;

	/** The source of the doors sound */
	UPROPERTY(Category = Door, VisibleAnywhere,
		BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* AudioSource;

	/** The curve to provide the alpha value when the door is opening or closing */
	UPROPERTY(Category = Door, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Motion"))
	UCurveFloat* MotionCurve;

	/** The timer handle for auto closing */
	FTimerHandle AutoCloseHandle;
};
