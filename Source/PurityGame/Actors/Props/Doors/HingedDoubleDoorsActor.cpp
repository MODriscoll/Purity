// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "HingedDoubleDoorsActor.h"

AHingedDoubleDoorsActor::AHingedDoubleDoorsActor()
{
	OppositeHinge = TYPE_OF_NULLPTR();
}

void AHingedDoubleDoorsActor::TickDoorMotion(float Alpha)
{
	if (Hinge && OppositeHinge)
	{
		// Set the hinge rotations based of alpha value
		FRotator NewRotation = FMath::Lerp(FRotator(0.f), FRotator(0.f, DesiredRotation, 0.f), FMath::Clamp(Alpha, 0.f, 1.f));
		Hinge->SetRelativeRotation(NewRotation);
		OppositeHinge->SetRelativeRotation(NewRotation.GetInverse() + FRotator(0.f, -180.f, 0.f));
	}
}
