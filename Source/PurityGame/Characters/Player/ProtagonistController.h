// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//class APuritySurvivalGameModeBase;

class AProtagonist;
class AProtagonistHUD;

class UItemAsset;

class UFocusTipWidget;
class UNotificationWidget;

#include "GameFramework/PlayerController.h"
#include "ProtagonistController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogProtagonistController, Log, All);

/**
* Controller for the protagonist class
*/
UCLASS()
class PURITYGAME_API AProtagonistController : public APlayerController
{
	GENERATED_BODY()

public:

	/** Initialise variables for the controller */
	AProtagonistController();

public:

	/** Applies value after potential invert to the pitch input rotation */
	virtual void AddPitchInput(float Val) override;

	/** Stores a reference to the protagonist */
	virtual void Possess(APawn* inPawn) override;

	/** Drops the reference to the protagonist */
	virtual void UnPossess() override;

	/** Disables the players ability to pull out the map and interact */
	virtual void SetCinematicMode(bool bInCinematicMode, bool bHidePlayer,
		bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning) override;

protected:

	/** Called when the game begins */
	virtual void BeginPlay() override;

	/** Sets up the input bindings */
	virtual void SetupInputComponent() override;

public:

	/** Returns the pointer to the protagonist being controlled */
	UFUNCTION(BlueprintPure, Category = "Protagonist")
	FORCEINLINE AProtagonist* GetProtagonist() const { return ControlledProtagonist; }

	/** Returns the pointer to the protagonists HUD */
	UFUNCTION(BlueprintPure, Category = "Protagonist")
	FORCEINLINE AProtagonistHUD* GetProtagonistHUD() const { return ProtagonistHUD; }

	/** Moves the protagonist forward */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void MoveProtagonistForward(float Value);

	/** Moves the protagonist right */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void MoveProtagonistRight(float Value);

	/** Attemps to interact with the world */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void Interact();

	/** Toggles the maps visibility */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void Unsheathe();

	/** Toggles if the game is paused */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void PauseGame();

	/** Set if the player is allowed to withdraw the map */
	UFUNCTION(BlueprintCallable, Category = "Protagonist")
	void SetCanUseMap(bool bCan);

	/** Helper function for setting the HUDs focus tip */
	UFUNCTION(BlueprintCallable, Category = "HUD")
	void SetFocusTip(const FText& Tip);

	/** Helper function for pushing a notification in the HUD */
	UNotificationWidget* PushNotification(float InLifeSpan, 
		TSubclassOf<UNotificationWidget> Template = nullptr);

	/** Along with pushing a notification, will also push
	and set the message and symbol for the generated notification */
	UFUNCTION(BlueprintCallable, Category = "HUD")
	UNotificationWidget* PushNotification(const FText& Notification, UTexture2D* Symbol, 
		float InLifeSpan, TSubclassOf<UNotificationWidget> Template = nullptr);

private:

	/** Checks if the player has the map item */
	bool HasMapItem() const;

	/** Called when the player has focused on an object */
	UFUNCTION()
	void OnObjectFocused(AActor* AsActor);

	/** Called when the player has unfocused on an object */
	UFUNCTION()
	void OnObjectUnfocused(AActor* AsActor);

	/** Called when the player collects a new item */
	UFUNCTION()
	void OnItemCollected(UItemAsset* ItemAdded, int32 IndexInInventory);

	/** Called when the player drops an item */
	UFUNCTION()
	void OnItemDropped(UItemAsset* ItemRemoved, int32 IndexInInventory);

public:

	/** If the player is allowed to interact with the world */
	UPROPERTY(Interp, BlueprintReadWrite, Category = "Controls")
	uint32 bCanInteract : 1;

	/** If movement should be allowed when the map is withdrawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controls")
	uint32 bCanMoveWhileMapWithdrawn : 1;

	/** If the players pitch rotation should be inverted */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controls")
	uint32 bInvertLookUp : 1;

private:

	/** If the player is allowed to withdraw the map */
	UPROPERTY(Interp, BlueprintReadOnly, Category = "Controls",
		meta = (AllowPrivateAccess = "true"))
	uint32 bCanUseMap : 1;

	/** Reference to the protaganist being controlled */
	UPROPERTY(Transient)
	AProtagonist* ControlledProtagonist;

	/** Reference to the HUD associated with this controller */
	UPROPERTY(Transient)
	AProtagonistHUD* ProtagonistHUD;

	/** Reference to the game mode of this game */
	//UPROPERTY(Transient)
	//APuritySurvivalGameModeBase* SurvivalGameMode;
};
