// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UImage;

#include "PopupWidget.h"
#include "MessagePopupWidget.generated.h"

/**
 * Widget that allows C++ classes to set an image
 * for a message to be displayed to the user. Bind
 * the desired image to utilise the set image
 */
UCLASS(abstract, meta = (DisplayName = "Message HUD"))
class PURITYGAME_API UMessagePopupWidget : public UPopupWidget
{
	GENERATED_BODY()
	
public:

	/** Set the image to display to the user */
	UFUNCTION(BlueprintCallable, Category = Readable)
	void SetMessage(UTexture2D* Texture);

protected:
	
	/** Gets the image to apply the message to */
	UFUNCTION(BlueprintImplementableEvent, Category = Readable)
	UImage* GetMessageImage() const;
};
