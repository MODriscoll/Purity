// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AMenuHUD;
class AMainMenuGameModeBase;

#include "GameFramework/PlayerController.h"
#include "MenuPlayerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMenuController, Log, All);

/**
 * Controller designed for use for by the main menu
 */
UCLASS()
class PURITYGAME_API AMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/** Stores a reference to the game mode */
	virtual void ReceivedGameModeClass(TSubclassOf<AGameModeBase> GameModeClass) override;

protected:

	/** Called when spawned into the world */
	virtual void BeginPlay() override;

private:

	/** Reference to the menu game mode */
	UPROPERTY(Transient)
	AMainMenuGameModeBase* MenuGameMode;

	/** Reference to the menu HUD */
	UPROPERTY(Transient)
	AMenuHUD* MenuHUD;
};
