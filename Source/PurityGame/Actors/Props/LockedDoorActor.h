// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UItemAsset;

#include "Actors/Props/DoorActor.h"
#include "Interfaces/ProtagonistCanInteractWith.h"
#include "LockedDoorActor.generated.h"

/**
* A door actor but with locking functionality that will
* require the player to have certains items before being unlocked.
* This actor should not be placed in the world, but should inherited from.
*/
UCLASS()
class PURITYGAME_API ALockedDoorActor : public ADoorActor, public IProtagonistCanInteractWith
{
	GENERATED_BODY()
	
public:
	
	/** Sets the default values */
	ALockedDoorActor();

public:

	/** Checks if the player has the required
	items if locked before opening the door */
	virtual void OnProtagonistInteract_Implementation(AProtagonistController* Player) override;

protected:
	
	/** Called when the game starts */
	virtual void BeginPlay() override;

	/** Locks the door again if lock on close is set to true */
	virtual void OnMotionFinish() override;

public:

	/** If this door is currently locked */
	bool IsLocked() const;

protected:

	/** Called when the door was successfully unlocked */
	UFUNCTION(BlueprintNativeEvent, Category = "Door|Lock")
	void OnUnlockDoorSuccess();
	virtual void OnUnlockDoorSuccess_Implementation() { }

	/** Called when door was failed to be unlocked */
	UFUNCTION(BlueprintNativeEvent, Category = "Door|Lock")
	void OnUnlockDoorFailure(const TArray<UItemAsset*>& RequiredItems);
	virtual void OnUnlockDoorFailure_Implementation(const TArray<UItemAsset*>& RequiredItems) { }

private:

	/** Unlocks this door if player has required items */
	bool Unlock(const AProtagonist* Protagonist);

public:

	/** If the door should be locked again when closed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door|Lock")
	uint32 bLockOnClose : 1;

	/** If the door was just unlocked via 
	interaction, should the door immediately open */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door|Lock")
	uint32 bOpenOnUnlock : 1;

	/** The items required to open this door */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door|Lock")
	TArray<UItemAsset*> RequiredToUnlock;

private:

	/** If this door is currenly locked */
	UPROPERTY(Category = "Door|Lock", BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	uint32 bIsLocked : 1;
};
