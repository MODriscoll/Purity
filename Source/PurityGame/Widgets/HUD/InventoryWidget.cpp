// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "InventoryWidget.h"

#include "Assets/Interactive/ItemAsset.h"

#include "Components/Image.h"

#include "WidgetBlueprintLibrary.h"

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UInventoryWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD Elements");
}

#endif

void UInventoryWidget::SetItemIcon(UItemAsset* Item, int32 Index)
{
	UImage* IconImage = GetItemIcon(Index);
	if (IconImage && Item)
	{
		UTexture2D* ItemImage = Item->GetItemIcon();
		if (ItemImage)
		{
			FSlateBrush IconBrush = IconImage->Brush;
			IconBrush.DrawAs = ESlateBrushDrawType::Image;
			IconBrush.SetResourceObject(ItemImage);
			IconBrush.ImageSize = FVector2D(ItemImage->GetSizeX(), ItemImage->GetSizeY());
			IconImage->SetBrush(IconBrush);
		}
	}
}

void UInventoryWidget::ClearItemIcon(int32 Index)
{
	UImage* IconImage = GetItemIcon(Index);
	if (IconImage)
	{
		FSlateBrush IconBrush = IconImage->Brush;
		IconBrush.DrawAs = ESlateBrushDrawType::NoDrawType;
		IconBrush.SetResourceObject(TYPE_OF_NULLPTR());
		IconImage->SetBrush(IconBrush);
	}
}

#undef LOCTEXT_NAMESPACE


