// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UCanvasPanel;
class UCanvasPanelSlot;
class UImage;

class UTexture2D;

#include "Blueprint/UserWidget.h"
#include "MinimapWidget.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMinimapWidget, Log, All);

/** TODO:
	Add a minimap marker struct that allows for world
	position and rotation to be set in. The minimap will
	create the struct and make a shared ptr to it. The
	minimap will auto update the markers on the map */

/**
 * Widget that can be used to represent a minimap. Any
 * widget can be used as Markers to display position on
 * the map. This widget requires a canvas but also has
 * built in functionality to support a default marker that
 * can be used for the owner of this minimap and the
 * ability for a map backdrop to be set.
*/
UCLASS(abstract, meta = (DisplayName = "Minimap Table"))
class PURITYGAME_API UMinimapWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

	/** Initialize this widget */
	virtual bool Initialize() override;

public:

	/** Set the backdrop for the minimap */
	UFUNCTION(BlueprintCallable, Category = Minimap)
	void SetMapBackdrop(UTexture2D* BackdropTexture);

	/** Set the backdrop for this minimap using custom size */
	UFUNCTION(BlueprintCallable, Category = Minimap)
	void SetMapBackdropWithSize(UTexture2D* BackdropTexture, const FVector2D& Size);

	/** Set the world position, size and direction for this minimap */
	UFUNCTION(BlueprintCallable, Category = Minimap)
	void SetWorldData(const FVector& WorldOrigin, const FVector& WorldSize, 
		const FRotator& WorldDirection = FRotator::ZeroRotator);

	/** Set the relative position of the default
	icon using the map related variables */
	UFUNCTION(BlueprintCallable, Category = "Minimap|Markers")
	void SetDefaultMarkerPosition(const FVector& WorldPosition);

	/** Set the relative position of an additional
	marker using the map related variables */
	UFUNCTION(BlueprintCallable, Category = "Minimap|Markers")
	void SetMarkerPosition(UWidget* Marker, const FVector& WorldPosition);

	/** Adds a new icon to this minimap and sets its relative position */
	UFUNCTION(BlueprintCallable, Category = "Minimap|Markers")
	UWidget* CreateMarker(const FVector& WorldPosition,
		TSubclassOf<UWidget> Template = nullptr);

	/** Removes an marker from the minimap */
	UFUNCTION(BlueprintCallable, Category = "Minimap|Icons")
	bool RemoveMarker(UWidget* Marker);

protected:

	/** Get the canvas to add additional markers to */
	UFUNCTION(BlueprintImplementableEvent, Category = Minimap)
	UCanvasPanel* GetMapCanvas() const;

	/** Get the image for the backdrop of the map */
	UFUNCTION(BlueprintImplementableEvent, Category = Minimap)
	UImage* GetMapBackdrop() const;

	/** Get the widget representing the default marker. This
	widget is assumed to be a child of the canvas returned
	by GetMapCanvas */
	UFUNCTION(BlueprintImplementableEvent, Category = "Minimap|Markers")
	UWidget* GetDefaultMarker() const;

private:

	/** Sets the position for this marker */
	void SetMarkerPosition(UCanvasPanelSlot* MarkerSlot, const FVector& WorldPosition);

public:

	/** The class to spawn as additional markers */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Minimap|Markers")
	TSubclassOf<UWidget> MarkerTemplate;

	/** The origin of this minimap in world space */
	UPROPERTY(Transient, BlueprintReadWrite, Category = Minimap)
	FVector MapWorldOrigin;

	/** The size of this minimap in world space */
	UPROPERTY(Transient, BlueprintReadWrite, Category = Minimap)
	FVector MapWorldSize;

	/** The direction of this minimap in world space */
	UPROPERTY(Transient, BlueprintReadOnly, Category = Minimap)
	FRotator MapWorldDirection;
};
