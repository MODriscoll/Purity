// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UImage;
class UItemAsset;

#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"

/**
 * Widget for displaying the icons of items. This
 * widget does not directly work with the inventory
 * component, but works via index like the inventory
 * component.
 */
UCLASS(abstract, meta = (DisplayName = "Inventory Grid"))
class PURITYGAME_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

public:

	/** Set the icon to represent the given item */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void SetItemIcon(UItemAsset* Item, int32 Index);

	/** Clears the icon for the indexed item */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void ClearItemIcon(int32 Index);

protected:

	/** Requests for the image representing the item of the given index */
	UFUNCTION(BlueprintImplementableEvent, Category = Inventory)
	UImage* GetItemIcon(int32 Index) const; /*PURE_VIRTUAL(
		UInventoryGridWidget::GetItemIcon, return TYPE_OF_NULLPTR(););*/
};
