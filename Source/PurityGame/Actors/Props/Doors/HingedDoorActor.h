// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Props/LockedDoorActor.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "HingedDoorActor.generated.h"

UENUM(BlueprintType)
enum class EHingeType : uint8
{
	/** The hinge rotates forward */
	Push,

	/** The hinge rotates backwards */
	Pull,
	
	/** The hinge will be pushed forward by the actor who opened it
	If no actor is set to have pushed it, clockwise will be used */
	AlwaysPush,

	/** The hinge will be pulled backwards by the actor who opened it 
	If no actor is set to have pulled it, anticlockwise will be used */
	AlwaysPull
};

/**
* Simple actor for a single hinged door the player can interact with.
* This actor includes a mesh for the door frame along with the door mesh.
*/
UCLASS()
class PURITYGAME_API AHingedDoorActor : public ALockedDoorActor, public IProtagonistCanFocusOn
{
	GENERATED_BODY()
	
public:

	/** Sets the default values */
	AHingedDoorActor();

public:

	/** Highlights this door in the world */
	virtual void OnProtagonistFocus_Implementation(AProtagonistController* Player) override { }

	/** Unhighlights this door in the world */
	virtual void OnProtagonistUnfocus_Implementation(AProtagonistController* Player) override { }

	/** Informs protagonist if this door is cosmetic or not */
	virtual bool CanBeFocused_Implementation() const override { return !bIsCosmetic && !IsSwinging(); }

	/** Returns a tip to give the protagonist */
	virtual FText GetFocusTip_Implementation() const override;

protected:

	/** Called when the game starts */
	virtual void BeginPlay() override;

protected:

	/** Calculates the sign for the desired rotation based on the players position */
	virtual void StartOpenDoor(const AActor* OpenedBy) override;

	/** Swings the door around based on the alpha value */
	virtual void TickDoorMotion(float Alpha) override;

public:

	/** The max rotation of this doors hinge */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Door, meta = (ClampMin = "0", ClampMax = "179"))
	float MaxRotation;

	/** The type of hinge this door has */
	UPROPERTY(Interp, BlueprintReadWrite, Category = Door)
	EHingeType SwingMode;

	/** The "hinge" of the door (This component will swing). */
	UPROPERTY(BlueprintReadWrite, Category = Door)
	USceneComponent* Hinge;

protected:

	/** The copy of MaxRotation but with the sign value set based on which way the door needs to rotate */
	float DesiredRotation;
};
