// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "Interfaces/ProtagonistCanInteractWith.h"
#include "InteractiveActor.generated.h"

/**
 * Base class for actors that can be interacted with.
 * Already derives from both protagonist interfaces
 */
UCLASS(abstract)
class PURITYGAME_API AInteractiveActor : public AActor, public IProtagonistCanFocusOn, public IProtagonistCanInteractWith
{
	GENERATED_BODY()

public:

	/** Called when the protagonist has focused on this object */
	virtual void OnProtagonistFocus_Implementation(AProtagonistController* Player) override { }

	/** Called when the protagonist has unfocused on this object */
	virtual void OnProtagonistUnfocus_Implementation(AProtagonistController* Player) override { }

	/** Returns if this interactive can be focused */
	virtual bool CanBeFocused_Implementation() const { return true; }

	/** Returns a tip to give the protagonist about this interactive */
	virtual FText GetFocusTip_Implementation() const;

	/** Called when the protagonist has interacted with this object */
	virtual void OnProtagonistInteract_Implementation(AProtagonistController* Player) override { }
};
