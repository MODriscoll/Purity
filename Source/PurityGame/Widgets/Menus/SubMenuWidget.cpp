// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "SubMenuWidget.h"

#include "Components/Button.h"

#include "Characters/Player/MenuHUD.h"

DEFINE_LOG_CATEGORY(LogSubMenuWidget)

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText USubMenuWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}

#endif

void USubMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// Automatically bind the return function
	// if a return button has been provided
	UButton* ReturnButton = GetReturnButton();
	if (ReturnButton)
	{
		ReturnButton->OnClicked.AddDynamic(this, &USubMenuWidget::ReturnToMainMenu);
	}
	else
	{
		UE_LOG(LogSubMenuWidget, Warning, TEXT("Submenu \"%s\" has not "
			"been provided. Could not automatically bind return function"), *GetName());
	}
}

void  USubMenuWidget::ReturnToMainMenu()
{
	if (MenuHUDReference)
	{
		MenuHUDReference->ClearSubMenu();
	}
	else
	{
		UE_LOG(LogSubMenuWidget, Warning, TEXT("No menu HUD has been set for sub menu \"%s\""), *GetName());
	}
}

#undef LOCTEXT_NAMESPACE


