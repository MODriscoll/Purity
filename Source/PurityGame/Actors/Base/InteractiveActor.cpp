// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "InteractiveActor.h"

#define LOCTEXT_NAMESPACE "Protagonist Focusable"

FText AInteractiveActor::GetFocusTip_Implementation() const
{
	return LOCTEXT("Focusable", "Interact");
}

#undef LOCTEXT_NAMESPACE
