// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MinimapWidget.h"

#include "WidgetTree.h"

#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"

#include "Kismet/KismetSystemLibrary.h"

DEFINE_LOG_CATEGORY(LogMinimapWidget);

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UMinimapWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD Elements");
}

#endif

bool UMinimapWidget::Initialize()
{
	bInitialized = Super::Initialize();
	if (bInitialized)
	{
		// There needs to be a canvas to add markers to
		UCanvasPanel* Map = GetMapCanvas();
		if (!Map)
		{
			bInitialized = false;
			return bInitialized;
		}

		UWidget* Marker = GetDefaultMarker();
		if (Marker)
		{
			if (!Map->HasChild(Marker))
			{
				UE_LOG(LogMinimapWidget, Warning, TEXT("Default Marker for Minimap \"%s\" is not child "
					"of map canvas. This can result in undefined behaviour"), *GetName());
			}
			else
			{
				UCanvasPanelSlot* MarkerSlot = Cast<UCanvasPanelSlot>(Marker->Slot);
				MarkerSlot->SetAnchors(FAnchors(0.f, 1.f));
			}
		}
	}

	return bInitialized;
}

void UMinimapWidget::SetMapBackdrop(UTexture2D* BackdropTexture)
{
	UImage* Backdrop = GetMapBackdrop();
	if (Backdrop)
	{
		FSlateBrush BackdropBrush = Backdrop->Brush;
		BackdropBrush.SetResourceObject(BackdropTexture);
		Backdrop->SetBrush(BackdropBrush);
	}
}

void UMinimapWidget::SetMapBackdropWithSize(UTexture2D* BackdropTexture, const FVector2D& Size)
{
	UImage* Backdrop = GetMapBackdrop();
	if (Backdrop)
	{
		FSlateBrush BackdropBrush = Backdrop->Brush;
		BackdropBrush.SetResourceObject(BackdropTexture);
		Backdrop->SetBrush(BackdropBrush);

		// Resize the canvas slot, instead of the image size
		UCanvasPanelSlot* BackdropSlot = Cast<UCanvasPanelSlot>(Backdrop->Slot);
		if (BackdropSlot)
		{
			BackdropSlot->SetSize(Size);
		}
	}
}

void UMinimapWidget::SetWorldData(const FVector& WorldOrigin, const FVector& WorldSize, const FRotator& WorldDirection)
{
	MapWorldOrigin = WorldOrigin;
	MapWorldSize = WorldSize;
	MapWorldDirection = WorldDirection;
}

void UMinimapWidget::SetDefaultMarkerPosition(const FVector& WorldPosition)
{
	UWidget* Marker = GetDefaultMarker();
	if (Marker)
	{
		UCanvasPanelSlot* MarkerSlot = Cast<UCanvasPanelSlot>(Marker->Slot);
		SetMarkerPosition(MarkerSlot, WorldPosition);
	}
}

void UMinimapWidget::SetMarkerPosition(UWidget* Marker, const FVector& WorldPosition)
{
	UCanvasPanel* Root = GetMapCanvas();
	if (Root)
	{
		bool bOwnsMarker = Root->HasChild(Marker);
		if (bOwnsMarker)
		{
			UCanvasPanelSlot* MarkerSlot = Cast<UCanvasPanelSlot>(Marker->Slot);
			SetMarkerPosition(MarkerSlot, WorldPosition);
		}
	}
}

UWidget* UMinimapWidget::CreateMarker(const FVector& WorldPosition,
	TSubclassOf<UWidget> Template /*= nullptr*/)
{
	// Make sure there is a valid widget to spawn
	{
		if (!Template)
		{
			Template = MarkerTemplate;
		}

		// Recheck incase the marker template is also invalid
		if (!UKismetSystemLibrary::IsValidClass(Template))
		{
			return TYPE_OF_NULLPTR();
		}
	}

	if (!WidgetTree)
	{
		UE_LOG(LogMinimapWidget, Fatal, TEXT("\"%s\" could not "
			"create new widget. WidgetTree is null"), *GetName());
		return TYPE_OF_NULLPTR();
	}

	// Create the new marker and attach it to the root
	UWidget* NewMarker = WidgetTree->ConstructWidget<UWidget>(Template);
	if (NewMarker)
	{
		UCanvasPanel* Root = GetMapCanvas();
		if (Root)
		{
			UCanvasPanelSlot* MarkerSlot = Root->AddChildToCanvas(NewMarker);

			// Set the new icon to draw in front of
			// the map but behind the default marker
			//MarkerSlot->SetAnchors(FAnchors(0.f, 1.f));
			MarkerSlot->SetAlignment(FVector2D(0.5f, 0.5f));
			MarkerSlot->SetZOrder(1);
			SetMarkerPosition(MarkerSlot, WorldPosition);
		}
	}

	return NewMarker;
}

bool UMinimapWidget::RemoveMarker(UWidget* Marker)
{
	UCanvasPanel* Root = GetMapCanvas();
	if (Root)
	{
		return Root->RemoveChild(Marker);
	}

	return false;
}

void UMinimapWidget::SetMarkerPosition(UCanvasPanelSlot* MarkerSlot, const FVector& WorldPosition)
{
	if (!MarkerSlot)
	{
		return;
	}

	// Convert the world position to a relative ratio
	FVector MarkerOffset = WorldPosition - MapWorldOrigin;
	MarkerOffset = MapWorldDirection.RotateVector(MarkerOffset);
	FVector EndOffset = MapWorldDirection.RotateVector(MapWorldSize);

	MarkerOffset.Z = 1.f;
	EndOffset.Z = 1.f;

	// Prevent division by zero
	{
		if (MarkerOffset.X == 0.f)
		{
			MarkerOffset.X = 1.f;
		}
		if (MarkerOffset.Y == 0.f)
		{
			MarkerOffset.Y = 1.f;
		}
	}

	MarkerOffset /= EndOffset;

	// Set the markers anchor so it stays relative
	// with the canvas when the canvas is resized
	MarkerSlot->SetAnchors(FAnchors(MarkerOffset.Y, 1.f - MarkerOffset.X));
}

#undef LOCTEXT_NAMESPACE


