// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "LockedDoorActor.h"

#include "Characters/Player/Protagonist.h"
#include "Characters/Player/ProtagonistController.h"

#include "Components/InventoryComponent.h"

ALockedDoorActor::ALockedDoorActor()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default variables
	bLockOnClose = false;
	bOpenOnUnlock = false;
}

void ALockedDoorActor::BeginPlay()
{
	Super::BeginPlay();

	if (!IsOpen())
	{
		bIsLocked = true;
	}
}

void ALockedDoorActor::OnProtagonistInteract_Implementation(AProtagonistController* Player)
{
	if (!bIsCosmetic)
	{
		const AProtagonist* Protagonist = Player->GetProtagonist();
		// Either open or close the door
		if (IsOpen())
		{
			Close(Protagonist);
		}
		else
		{
			bool bShouldOpen = true;

			// If door is locked, check if player has the required items
			if (IsLocked())
			{
				bool bWasUnlocked = Unlock(Protagonist);
				bShouldOpen = bWasUnlocked && bOpenOnUnlock;
				//ShouldOpen = !bIsLocked && bOpenOnUnlock;
			}

			if (bShouldOpen)
			{
				Open(Protagonist);
			}
		}
	}
}

void ALockedDoorActor::OnMotionFinish()
{
	Super::OnMotionFinish();

	if (bLockOnClose)
	{
		bIsLocked = true;
	}
}

bool ALockedDoorActor::IsLocked() const
{
	// This door is only ever considered lock
	// if there are requirements in order to unlock
	return bIsLocked && RequiredToUnlock.Num();
}

bool ALockedDoorActor::Unlock(const AProtagonist* Protagonist)
{
	// Find out if player meets item requirements
	UInventoryComponent* Inventory = Protagonist->GetItemInventory();
	FInventoryQuery Query(RequiredToUnlock);
	bool MeetsRequirements = Inventory->QueryItems(Query);

	if (MeetsRequirements)
	{
		OnUnlockDoorSuccess();
	}
	else
	{
		OnUnlockDoorFailure(Query.GetMissingItems());
	}

	bIsLocked = !MeetsRequirements;
	return MeetsRequirements;
}


