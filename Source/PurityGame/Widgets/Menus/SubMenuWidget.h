// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UButton;

class AMenuHUD;

#include "Blueprint/UserWidget.h"
#include "SubMenuWidget.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSubMenuWidget, Log, All);

/**
 * A widget that is designed to work with the main menu.
 * Allows the for the binding of a return button that will
 * lead the user back to the main menu of the game
 */
UCLASS(abstract, meta = (DisplayName = "Sub Menu"))
class PURITYGAME_API USubMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

#if WITH_EDITOR

	/** The palette category of this widget */
	virtual const FText GetPaletteCategory() override;

#endif

protected:

	/** Called when the underlaying slate has been constructed */
	virtual void NativeConstruct() override;

protected:

	/** Get the button to bind the return event to */
	UFUNCTION(BlueprintImplementableEvent, Category = Menu)
	UButton* GetReturnButton() const;
	
	
	/** Returns the user back to the main menu */
	UFUNCTION(BlueprintCallable, Category = Menu)
	void ReturnToMainMenu();

public:

	/** Reference to the main menu HUD that owns this sub menu */
	UPROPERTY(Transient)
	AMenuHUD* MenuHUDReference;
};
