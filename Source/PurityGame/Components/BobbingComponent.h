// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "Utility/BobbingTypes.h"
#include "BobbingComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBobbingComponent, Log, All);

/** Delegate for when the bobbing reaches an apex */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBobApexSignature, EBobbingApex, Apex);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PURITYGAME_API UBobbingComponent final : public USceneComponent
{
	GENERATED_BODY()

public:	

	/** Sets default values for this component's properties */
	UBobbingComponent();

protected:

	/** Called when the game starts */
	virtual void BeginPlay() override final;

public:	

	/** Called when the attached parent is changed */
	virtual void OnAttachmentChanged() override final;

	/** Called every frame */
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override final;

public:
		
	/** Add input value to be appilied next frame */
	UFUNCTION(BlueprintCallable, Category = Bobbing)
	void AddInput(float Value);

	/** Returns the speed of the bobbing */
	UFUNCTION(BlueprintCallable, Category = Bobbing)
	float GetSpeed() const { return BobbingSpeed; }

	/** Reset the bobbing to the parents location */
	UFUNCTION(BlueprintCallable, Category = Bobbing)
	void Reset();

	/** Sets the speed of the bobbing */
	UFUNCTION(BlueprintCallable, Category = Bobbing)
	void SetSpeed(float NewSpeed);
	
private:

	/** Returns the delta input value after resetting it */
	float ConsumeInput();

	/** Returns the sign apex as a enum value */
	EBobbingApex GetBobbingApex() const;

	/** Increase the running time of the bobbing */
	void UpdateRunningTime(float Scale, float DeltaTime);

	/** Updates the sign value for checking the bobs apex */
	void UpdateApexSign();

public:

	/** If the bobbing should re-center itself when no input was made */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bobbing)
	uint32 bAutoCenter : 1;

	/** The apex of the bobbing (in both directions) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bobbing, meta = (DisplayName = "Apex"))
	float BobbingDiameter;

	/** The delegate that will be called when an apex of the bobbing is reached 
		@param1: The apex reahced. This value can only be either high or low */
	UPROPERTY(BlueprintAssignable, Category = Bobbing)
	FBobApexSignature OnApexReached;

private:
	
	/** The speed of the bobbing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bobbing, meta = (DisplayName = "Speed", AllowPrivateAccess = "true"))
	float BobbingSpeed;

	/** The progress of the bobbing effect */
	float RunningTime;

	/** The input during a frame to be applied next tick */
	float DeltaInput;

	/** The unit sign to multiply half pi with when checking apex */
	float ApexSignValue;
};
