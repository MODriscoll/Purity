// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AProtagonist;
class AProtagonistController;

#include "ProtagonistCanFocusOn.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UProtagonistCanFocusOn : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * Interface that allows the protagonist to call to notify when
 * the player has started to look or no longer looking at them.
 */
class PURITYGAME_API IProtagonistCanFocusOn
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	/** Called when the protagonist has started focusing on this object */
	/** @param1: The controller for the protagonist */
	UFUNCTION(BlueprintNativeEvent, Category = "Focusing")
	void OnProtagonistFocus(AProtagonistController* Player);

	/** Called when the protagonist has stopped focusing on this object */
	/** @param1: The controller for the protagonist */
	UFUNCTION(BlueprintNativeEvent, Category = "Focusing")
	void OnProtagonistUnfocus(AProtagonistController* Player);

	/** Called when the protagonist is about to focus on this object */
	/**	Is used to determine if OnFocus should be called */
	UFUNCTION(BlueprintNativeEvent, Category = "Focusing")
	bool CanBeFocused() const;

	/** The tip to give the protagonist when focusing on this object */
	UFUNCTION(BlueprintNativeEvent, Category = "Focusing")
	FText GetFocusTip() const;
};
