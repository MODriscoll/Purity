// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class ULoadingWidget;

#include "GameFramework/GameModeBase.h"
#include "MainMenuGameModeBase.generated.h"

/** Delegate for when a level has finished loading */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLevelLoadCompletedSignature, const FName&, LevelName);

/**
 * Game mode to be used as the base for the main menu level. Offers
 * the functionality to loa
 */
UCLASS()
class PURITYGAME_API AMainMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	/** Sets default values */
	AMainMenuGameModeBase();

public:

	/** Loads a new level */
	UFUNCTION(BlueprintCallable, Category = "Main Menu")
	bool LoadLevel(const FString& PackageName);

	/** Unloads the current level reference */
	UFUNCTION(BlueprintCallable, Category = "Main Menu")
	void UnloadLevel();

	/** Get the level being loaded */
	UFUNCTION(BlueprintPure, Category = "Main Menu")
	const FStringAssetReference& GetLevelReference() const { return PackageReference; }

private:

	/** Called when a package finishes loading */
	UFUNCTION()
	void OnLevelFinishedLoading();

public:

	/** Delegate for when the level
	was loaded in successfully */
	FLevelLoadCompletedSignature OnLoadSuccessfull;

	/** Delegate for when the level
	was failed to be loaded */
	FLevelLoadCompletedSignature OnLoadFailed;

public:

	/** The loading widget to display when loading a level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Menu)
	TSubclassOf<ULoadingWidget> LoadTemplate;

private:

	/** Reference to the loading widget */
	UPROPERTY(Transient)
	ULoadingWidget* LoadWidgetReference;

	/** Streamer responsible for loading the level */
	FStreamableManager PackageStreamer;

	/** The reference to the package being loaded */
	FStringAssetReference PackageReference;
};
