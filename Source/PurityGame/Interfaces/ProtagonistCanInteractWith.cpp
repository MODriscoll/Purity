// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ProtagonistCanInteractWith.h"


// This function does not need to be modified.
UProtagonistCanInteractWith::UProtagonistCanInteractWith(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IProtagonistCanInteractWith functions that are not pure virtual.
