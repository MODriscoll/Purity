// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UItemAsset;

class UWidget;
class UFocusTipWidget;
class UNotificationWidget;
class UNotificationListWidget;
class UPopupWidget;
class UMapPopupWidget;
class UPauseMenuWidget;

#include "GameFramework/HUD.h"
#include "ProtagonistHUD.generated.h"

USTRUCT(BlueprintType)
struct PURITYGAME_API FMessageSettings
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FMessageSettings()
	{
		bInstantlyToggle = false;
		bStallToggleRequest = false;
	}

public:

	/** If the message should instantly
	toggle between hidden and withdrawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup)
	uint32 bInstantlyToggle : 1;

	/** If toggle should be ignored when
	the message is still in popping motion */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup,
		meta = (EditCondition = "!bInstantlyToggle"))
	uint32 bStallToggleRequest : 1;
};

USTRUCT()
struct PURITYGAME_API FMessageWrapper
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FMessageWrapper()
	{
		Message = TYPE_OF_NULLPTR();
	}

public:

	/** Resets this wrapper */
	void Reset();

public:

	/** The message being wrapped */
	UPROPERTY()
	UPopupWidget* Message;

	/** Settings for the wrapped message */
	UPROPERTY()
	FMessageSettings Settings;

private:

	friend class AProtagonistHUD;

	/** Handle for the message hidden event */
	FDelegateHandle Handle;
};

DECLARE_LOG_CATEGORY_EXTERN(LogProtagonistHUD, Log, All);

/**
 * The HUD to be used by the protagonist controller. Allows
 * for interaction between the controller and the HUD widgets
 */
UCLASS()
class PURITYGAME_API AProtagonistHUD : public AHUD
{
	GENERATED_BODY()

public:

	/** Sets default values */
	AProtagonistHUD();

protected:

	/** Called when the game starts */
	virtual void BeginPlay() override;

	/** Called when the game has ended */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:

	/** Called every frame */
	virtual void DrawHUD() override;

	/** Toggles if HUD should be dranw */
	virtual void ShowHUD() override;
	
public:

	/** Get the map directly */
	UMapPopupWidget* GetMap() const { return MapReference; }

	/** Get the pause widget directly. This can be null */
	UPauseMenuWidget* GetPauseWidget() const { return PauseReference; }

	/** Sets the focus tip to display the player,
	if the tip is emtpy, the tip will be removed */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void SetFocusTip(const FText& Tip);

	/** Pushes a new notification to the notification list */
	UFUNCTION(BlueprintCallable, Category = HUD)
	UNotificationWidget* PushNotification(float LifeSpan,
		TSubclassOf<UNotificationWidget> Template = nullptr);

	/** Will withdraw or hide the map. If the displayed
	message is not the map, the map will be queued */
	UFUNCTION(BlueprintCallable, Category = "HUD|Map")
	void ToggleMap();

	/** Displays the given message to the user, returns
	if this message was either displayed or queued to be */
	UFUNCTION(BlueprintCallable, Category = HUD,
		meta = (HasNativeMake = "true"))
	bool ShowMessage(UPopupWidget* Message, bool bDisplayImmediately, 
		const FMessageSettings& InSettings);

	/** Hides the displayed message. Forcing will
	ignore the settings of the displayed message */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void HideMessage(bool bForceInstantly = true);

	/** Hides all the displayed widgets on the screen instantly */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void HideAll();

	/** Toggles the pause menu. If pause menu is set to handle input
	settings by default. This function could pause/unpause the game */
	UFUNCTION(BlueprintCallable, Category = "HUD|Pause")
	void TogglePauseMenu();

	/** Returns if the displayed message is withdrawn */
	UFUNCTION(BlueprintPure, Category = HUD)
	bool IsMessageWithdrawn() const;

	/** Returns if the message is visible to the user */
	UFUNCTION(BlueprintPure, Category = HUD)
	bool IsMessageVisible() const;

	/** If a message is already in the queue */
	UFUNCTION(BlueprintPure, Category = HUD)
	bool IsMessageQueued() const { return !!QueuedMessage.Message; }

	/** If the displayed message is currently the map */
	UFUNCTION(BlueprintPure, Category = "HUD|Map")
	bool IsMessageTheMap() const;

	/** Sets the item icon in the inventory
	widget using the given item. If item is
	null, the icon index is cleared instead */
	UFUNCTION(BlueprintCallable, Category = "HUD|Inventory")
	void ToggleInventoryItemIcon(UItemAsset* Item, int32 Index) const;

	/** Set the players marker on the minimap */
	UFUNCTION(BlueprintCallable, Category = "HUD|Minimap")
	void SetProtagonistMarkerPosition(const FVector& Position) const;

	/** Get the default crosshair for the HUD */
	static UTexture2D* GetDefaultCrosshair();

private:

	/** Toggles the state of the displayed message */
	void ToggleMessage();



	/** Creates the map widget, returns if creation was successfull */
	bool CreateMapWidget(bool bOverwrite = false);

	/** Creates the pause widget, returns if creation was successfull */
	bool CreatePauseWidget();



	/** Starts displaying the message */
	void StartMessageWithdraw(bool bInit);
	
	/** Ends displaying the message */
	void StartMessageHide();

	/** Starts displaying the pause menu */
	void StartPauseDisplay();

	/** Ends displaying the pause menu */
	void StartPauseDestroy();



	/** Called when the displayed message has finished hiding */
	UFUNCTION()
	void OnMessageHidden();

	/** Called by the pause widget when starting to fade out */
	UFUNCTION()
	void PauseStartedFadingOut();

	/** Called by the pause widget when fishing fading out */
	UFUNCTION()
	void PauseFinishedFadingOut();



	/** Plays a popup sound effect after stopping the current audio.
	Assumes this audio is a sound effect for the map popup widget */
	void PlayPopupSoundEffect(USoundBase* SoundEffect) const;

	/** Plays a pause sound effect after pausing the popup sound effect
	if playing, Assumes this audio is a sound effect for the pause widget */
	void PlayPauseSoundEffect(USoundBase* SoundEffect) const;

	/** Helper functions for setting visibility of widgets based on if HUD is
	being drawn. These functions will check if the widget is still valid */
	void SetWidgetVisibility(UWidget* Widget) const;
	void SetWidgetVisibility(UWidget* Widget, ESlateVisibility Visibility) const;

public:

	/** The focus tip widget to create and manage when hinting the player */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD,
		meta = (DisplayName = "Focus Tip Template"))
	TSubclassOf<UFocusTipWidget> TipTemplate;

	/** The notification list template to create and manage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD,
		meta = (DisplayName = "Notification List Template"))
	TSubclassOf<UNotificationListWidget> NotifyListTemplate;

	/** The map widget to create and manage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	TSubclassOf<UMapPopupWidget> MapTemplate;

	/** The pause widget to create and manage when game is paused */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	TSubclassOf<UPauseMenuWidget> PauseTemplate;

	/** If the crosshair should be drawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	uint32 bDrawCrosshair : 1;

	/** The settings to use when displaying the map */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Map)
	FMessageSettings MapMessageSettings;

	/** If input mode and pause settings should
	be handled when toggling the pause widget */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pause)
	uint32 bHandlePauseInputSettings : 1;

	/** If the players icon should be updated every tick. This
	assumes that the owner is the player requested to be tracked */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD|Minimap",
		meta = (DisplayName = "Track Protagonist on Minimap (NYI)"))
	uint32 bTrackProtagonist : 1;

	/** The sound to be played when pausing the game */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pause)
	USoundBase* PauseSound;

	/** The sound to be played when unpausing the game */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pause)
	USoundBase* UnpauseSound;

	/** The texture representing the crosshair */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	UTexture2D* Crosshair;

private:

	/** The audio component for playing the popup sound effects */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Map,
		meta = (AllowPrivateAccess = "true"))
	UAudioComponent* MessageAudio;

	/** The popup sound that was paused when pausing the game */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pause,
		meta = (AllowPrivateAccess = "true"))
	UAudioComponent* PauseAudio;

	/** Reference to the focus tip widget when instanced */
	UPROPERTY()
	UFocusTipWidget* TipReference;

	/** Reference to the notification list when instanced */
	UPROPERTY()
	UNotificationListWidget* NotifyListReference;

	/** Reference to the map widget */
	UPROPERTY(BlueprintReadOnly, Category = HUD, 
		meta = (AllowPrivateAccess = "true"))
	UMapPopupWidget* MapReference;

	/** Reference to the pause widget when instanced */
	UPROPERTY(BlueprintReadOnly, Category = HUD,
		meta = (AllowPrivateAccess = "true"))
	UPauseMenuWidget* PauseReference;

	/** The message being displayed to the user */
	FMessageWrapper DisplayedMessage;

	/** The message waiting to be displayed */
	FMessageWrapper QueuedMessage;
};
