// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "PopupWidget.h"

#include "WidgetAnimation.h"

DEFINE_LOG_CATEGORY(LogPopupWidget);

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UPopupWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD");
}

#endif

void UPopupWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	// We are only concerned for the pop animation.
	// Don't call the events on other animations
	if (Animation == GetPopAnimation() && !bSkipOnFinished)
	{
		if (bIsWithdrawn)
		{
			OnWithdrawnFinished.Broadcast();
		}
		else
		{
			OnHiddenFinished.Broadcast();
		}
	}
}

void UPopupWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsWithdrawn = false;
	bSkipOnFinished = false;
	PausedTime = 0.f;
}

bool UPopupWidget::IsPopping() const
{
	return IsAnimationPlaying(GetPopAnimation());
}

void UPopupWidget::Withdraw(bool bInstanly /*= false*/, bool bForce /*= false*/)
{
	if ((!bIsWithdrawn || bForce) &&
		PlayPopAnimation(EUMGSequencePlayMode::Forward, bInstanly))
	{
		bIsWithdrawn = true;
	}
}

void UPopupWidget::Hide(bool bInstanly /*= false*/, bool bForce /*= false*/)
{
	if ((bIsWithdrawn || bForce) &&
		PlayPopAnimation(EUMGSequencePlayMode::Reverse, bInstanly))
	{
		bIsWithdrawn = false;
	}
}

void UPopupWidget::PausePop()
{
	UWidgetAnimation* PopAnimation = GetPopAnimation();
	if (PopAnimation)
	{
		if (IsAnimationPlaying(PopAnimation))
		{
			PausedTime = PauseAnimation(PopAnimation);

			// If animation is playing in reverse, we
			// need to flip the time to work backwards
			if (!bIsWithdrawn)
			{
				PausedTime = PopAnimation->GetEndTime() - PausedTime;
			}
		}
	}
}

void UPopupWidget::ResumePop()
{
	UWidgetAnimation* PopAnimation = GetPopAnimation();
	if (PopAnimation)
	{
		// Make sure time was actually paused between valid range
		if (PausedTime > PopAnimation->GetStartTime() &&
			PausedTime < PopAnimation->GetEndTime())
		{
			EUMGSequencePlayMode::Type PlayMode = bIsWithdrawn ?
				EUMGSequencePlayMode::Forward : EUMGSequencePlayMode::Reverse;
			PlayAnimation(PopAnimation, PausedTime, 1, PlayMode);

			// To prevent animation from restarting if
			// resume is called without calling pause
			PausedTime = PopAnimation->GetStartTime();
		}
	}
}

bool UPopupWidget::PlayPopAnimation(EUMGSequencePlayMode::Type PlayMode, bool bInstanly)
{
	UWidgetAnimation* PopAnimation = GetPopAnimation();
	if (PopAnimation)
	{
		// We can skip this part if we are instanly popping
		if (IsAnimationPlaying(PopAnimation) && !bInstanly)
		{
			// Need to set skip first before
			bSkipOnFinished = true;

			// Get the total and current time of the animation
			float TotalTime = PopAnimation->GetEndTime();
			float CurrentTime = GetAnimationCurrentTime(PopAnimation);

			// If animation is requested to play in same
			// mode as its playing right now, leave it be
			if (bIsWithdrawn != (PlayMode == EUMGSequencePlayMode::Forward))
			{
				// If animation is playing reversing now and requested
				// to play forward, we need to flip the current time to
				// accomadate the fact that time is being reduced from total time
				if (PlayMode == EUMGSequencePlayMode::Forward)
				{
					CurrentTime = TotalTime - CurrentTime;
				}

				PlayAnimation(PopAnimation, TotalTime - CurrentTime, 1, PlayMode);
			}

			bSkipOnFinished = false;
		}
		else
		{
			float StartAt = bInstanly ? 1.f : 0.f;
			PlayAnimation(PopAnimation, StartAt, 1, PlayMode);
		}

		return true;
	}
	else
	{
		UE_LOG(LogPopupWidget, Warning, TEXT("Could not play animation "
			"for \"%s\". Pop animation was null"), *GetName());
		return false;
	}
}

#undef LOCTEXT_NAMESPACE


