// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SoundEmitterActor.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSoundEmitterActor, Log, All);

USTRUCT(BlueprintType)
struct PURITYGAME_API FSoundSegment
{
	GENERATED_BODY()

public:

	/** Sets default values */
	FSoundSegment()
	{
		Sound = TYPE_OF_NULLPTR();
		DelayAfter = 5.f;
	}

public:

	/** The sound to play this segment */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* Sound;

	/** The delay after before switching segments */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DelayAfter;
};

UCLASS()
class PURITYGAME_API ASoundEmitterActor : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Sets default values for this actor's properties */
	ASoundEmitterActor();

protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

private:

	/** Prapares and plays the next segment */
	UFUNCTION()
	void PlayNextSegment();

	/** Prepares the delay before starting next segment */
	UFUNCTION()
	void DelayNextSegment();

public:

	/** The initial delay after emitter is created */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SoundEmitter)
	float InitialDelay;

	/** If the flicker segments should be followed in order and loop */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SoundEmitter)
	uint32 bInOrder : 1;

	/** If the same segment should be used twice in a row if random */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SoundEmitter, meta = (EditCondition = "!bInOrder"))
	uint32 bRepeatSegment : 1;

private:

	/** The index of the segment being emitted */
	int32 SegmentIndex;

	/** The source of the sounds to emit */
	UPROPERTY(Category = SoundEmitter, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* EmitterSource;

	/** The sound segments for the emitter to play */
	UPROPERTY(Category = SoundEmitter, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<FSoundSegment> SoundSegments;

	/** The randomiser stream for random selection of segments */
	UPROPERTY(Category = SoundEmitter, EditInstanceOnly, meta = (AllowPrivateAccess = "true"))
	FRandomStream SegmentRandomiser;

	/** The handle for the delays between segments */
	FTimerHandle DelayHandle;
};

