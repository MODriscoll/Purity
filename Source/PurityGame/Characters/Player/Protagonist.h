// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AProtagonistController;

class IProtagonistCanFocusOn;

class UBobbingComponent;
class UInteractorComponent;
class UInventoryComponent;

class UItemAsset;

#include "GameFramework/Character.h"
#include "Components/Utility/BobbingTypes.h"
#include "Protagonist.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogProtagonist, Log, All);

UCLASS()
class PURITYGAME_API AProtagonist : public ACharacter
{
	GENERATED_BODY()

public:

	/** Sets default values for this character's properties */
	AProtagonist();

public:	

	/** Called to bind functionality to input */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

public:

	/** Move the protagonist forward */
	void MoveForward(float Value);

	/** Move the protagonist right */
	void MoveRight(float Value);

	/** Rotate the players pitch up or down */
	void LookUp(float Value);

	/** Rotate the players yaw left or right */
	void TurnAround(float Value);

	/** Interact with the interactive found by the interactor */
	void Interact(AProtagonistController* ProtagController);

	/** Attempts to add an item to the players inventory */
	UFUNCTION(BlueprintCallable, Category = Character)
	bool AddItemToInventory(UItemAsset* Item, int32& OutIndex);

	/** Plays a randomly selected footstep sound */
	void PlayRandomFootstepSound();

	/** Returns this protagonists bobbing component */
	FORCEINLINE UBobbingComponent* GetHeadBobber() const { return HeadBobber; }

	/** Returns the protagonists first person camera */
	FORCEINLINE UCameraComponent* GetFPSCamera() const { return FPSCamera; }

	/** Returns the source of the protagonists footsteps */
	FORCEINLINE UAudioComponent* GetFootstepSource() const { return FootstepSource; }

	/** Returns the protagonists world interactor */
	FORCEINLINE UInteractorComponent* GetWorldInteractor() const { return WorldInteractor; }

	/** Returns the protagonists item inventory */
	FORCEINLINE UInventoryComponent* GetItemInventory() const { return ItemInventory; }

	/** Returns the protagonists key item inventory */
	FORCEINLINE UInventoryComponent* GetKeyItemInventory() const { return KeyItemInventory; }

public:

	/** The apex(s) on which a footstep sound should play */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Sounds",
		meta = (Bitmask, BitmaskEnum = EBobbingApex))
	uint8 PlayFootstepOn;

	/** If the footstep sounds should play in order */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Sounds")
	uint32 bPlayFootstepsInOrder : 1;

	/** The footstep sounds that can be played */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Sounds")
	TArray<USoundBase*> FootstepSounds;

private:

	/** Applys movement along the given axes to the player */
	void Move(float Value, EAxis::Type Axis);

	/** Called when the character moves, applies bobbing input */
	UFUNCTION()
	void OnCharacterMovement(float DeltaTime, FVector InitialLocation, FVector InitialVelocity);

	/** Called when the protagonists reaches a bobbing apex, plays the footstep source */
	UFUNCTION()
	void OnBobbingApexReached(EBobbingApex Apex);

private:

	/** Index of the footstep sound being played */
	int32 FootstepSoundIndex;

	/** The bobbing component being used to simulate the head bobbing while walking */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBobbingComponent* HeadBobber;

	/** The camera component for the players view port of the world */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FPSCamera;

	/** The audio source for the footstep sounds when the player walks */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* FootstepSource;

	/** The interactor component for the player to be able to interact with the world */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInteractorComponent* WorldInteractor;

	/** The inventory for the player to store their items */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventoryComponent* ItemInventory;

	/** The inventory for the player to store key items */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventoryComponent* KeyItemInventory;
};
