// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BobbingTypes.generated.h"

UENUM(BlueprintType, meta = (Bitflags))
enum class EBobbingApex : uint8
{
	ApexHigh UMETA(DisplayName = "Top"),
	ApexLow	 UMETA(DisplayName = "Bottom")
};

ENUM_CLASS_FLAGS(EBobbingApex)