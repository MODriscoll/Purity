// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MarkerWidget.h"

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText UMarkerWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD Elements");
}

#endif

#undef LOCTEXT_NAMESPACE


