// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "Protagonist.h"

#include "ProtagonistController.h"

#include "Components/BobbingComponent.h"
#include "Components/InteractorComponent.h"
#include "Components/InventoryComponent.h"

#include "Interfaces/ProtagonistCanInteractWith.h"

#include "Assets/Interactive/ItemAsset.h"

#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY(LogProtagonist);

AProtagonist::AProtagonist()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default values
	PlayFootstepOn = 1 << static_cast<uint8>(EBobbingApex::ApexLow);
	bPlayFootstepsInOrder = true;

	UCapsuleComponent* CollisionCapsule = GetCapsuleComponent();

	// Create the head bobbing component and attach it to the capsule
	HeadBobber = CreateDefaultSubobject<UBobbingComponent>(TEXT("Head Bobber"));
	HeadBobber->SetupAttachment(CollisionCapsule);

	// Set the bobber to default position around the eyes
	HeadBobber->SetRelativeLocation(FVector(0.f, 0.f, BaseEyeHeight + 40.f));

	/** Create the footstep source and attach it to the capsule */
	FootstepSource = CreateDefaultSubobject<UAudioComponent>(TEXT("Footstep Source"));
	FootstepSource->SetupAttachment(CollisionCapsule);

	// Set the footsteps to play from the bottom of the character
	FootstepSource->SetRelativeLocation(
		FVector(0.f, 0.f, -CollisionCapsule->GetUnscaledCapsuleHalfHeight()));

	// Set default settings for the footsteps
	FootstepSource->bIsUISound = false;
	FootstepSource->bIsMusic = false;
	FootstepSource->bAutoDestroy = false;
	FootstepSource->bSuppressSubtitles = true;

	// Create the fps camera component and attach it to the head bobber
	FPSCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FPS Camera"));
	FPSCamera->SetupAttachment(HeadBobber);

	// Set the camera to inherit the controllers rotation
	FPSCamera->bUsePawnControlRotation = true;

	// Create the world interactor component and attach it to the camera
	WorldInteractor = CreateDefaultSubobject<UInteractorComponent>(TEXT("World Interactor"));
	WorldInteractor->SetupAttachment(FPSCamera);

	// Set the interactor to inherit controllers rotation
	WorldInteractor->bUsePawnControlRotation = true;

	// Create the item inventory and attach it to the protagonist
	ItemInventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Item Inventory"));
	AddOwnedComponent(ItemInventory);

	// Create the key item inventory and attach it to the protagonist
	KeyItemInventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Key Item Inventory"));
	AddOwnedComponent(KeyItemInventory);

	// Bind the bobbing to add input when moving
	OnCharacterMovementUpdated.AddDynamic(this, &AProtagonist::OnCharacterMovement);

	// Bind the footsteps to play a footstep sound on a bob
	HeadBobber->OnApexReached.AddDynamic(this, &AProtagonist::OnBobbingApexReached);
}

void AProtagonist::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind axes
	PlayerInputComponent->BindAxis("LookUp", this, &AProtagonist::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &AProtagonist::TurnAround);
}

void AProtagonist::BeginPlay()
{
	Super::BeginPlay();

	// Index is set to negative one so PlayRandomFootstep
	// can set it to zero after the first valid bob
	FootstepSoundIndex = -1;
}

void AProtagonist::MoveForward(float Value)
{
	Move(Value, EAxis::X);
}

void AProtagonist::MoveRight(float Value)
{
	Move(Value, EAxis::Y);
}

void AProtagonist::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void AProtagonist::TurnAround(float Value)
{
	AddControllerYawInput(Value);
}

void AProtagonist::Interact(AProtagonistController* ProtagController)
{
	// Interact with the world if an interactive is in focus
	AActor* Interactive = WorldInteractor->GetFoundInteractive();
	if (Interactive && ProtagController &&
		Interactive->GetClass()->ImplementsInterface(UProtagonistCanInteractWith::StaticClass()))
	{
		IProtagonistCanInteractWith::Execute_OnProtagonistInteract(Interactive, ProtagController);
	}
}

bool AProtagonist::AddItemToInventory(UItemAsset* Item, int32& OutIndex)
{
	bool bAdded = false;
	OutIndex = -1;

	if (Item)
	{
		// Add the items to the appropriate inventory
		if (Item->IsKeyItem())
		{
			bAdded = KeyItemInventory->AddItem(Item, OutIndex);
		}
		else
		{
			bAdded = ItemInventory->AddItem(Item, OutIndex);
		}
	}

	return bAdded;
}

void AProtagonist::PlayRandomFootstepSound()
{
	if (FootstepSounds.Num() > 0)
	{
		if (bPlayFootstepsInOrder)
		{
			// Keep the sound index within a valid range
			(++FootstepSoundIndex) %= FootstepSounds.Num();
		}
		else
		{
			FootstepSoundIndex = UKismetMathLibrary::RandomIntegerInRange(0, FootstepSounds.Num() - 1);
		}

		USoundBase* Footstep = FootstepSounds[FootstepSoundIndex];
		FootstepSource->SetSound(Footstep);

		// If footstep didn't auto play, play it now
		if (!FootstepSource->IsPlaying())
		{
			FootstepSource->Play();
		}
	}
	else
	{
		UE_LOG(LogProtagonist, Warning,
			TEXT("No footstep sounds have been assigned for \"%s\". Could not play footstep sound"), *GetName());
	}
}

void AProtagonist::OnCharacterMovement(float DeltaTime, FVector InitialLocation, FVector InitialVelocity)
{
	const UCharacterMovementComponent* Movement = GetCharacterMovement();

	if (!Movement->IsFalling())
	{
		// Calculate the percentage of the players speed
		float Percentage = InitialVelocity.SizeSquared() / FMath::Pow(Movement->MaxWalkSpeed, 2.f);
		HeadBobber->AddInput(Percentage);
	}
}

void AProtagonist::OnBobbingApexReached(EBobbingApex Apex)
{
	if ((PlayFootstepOn & (1 << static_cast<uint8>(Apex))) != 0)
	{
		PlayRandomFootstepSound();
	}
}

void AProtagonist::Move(float Value, EAxis::Type Axis)
{
	// Using and applying only yaw rotation,
	// move the protagonist along the given axis
	FRotator Rotation = GetControlRotation();
	FRotator YawOnly(0.f, Rotation.Yaw, 0.f);
	FVector Unit = FRotationMatrix(YawOnly).GetUnitAxis(Axis);
	AddMovementInput(Unit, Value);
}



