// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "LoadingWidget.h"

#define LOCTEXT_NAMESPACE "UMG"

#if WITH_EDITOR

const FText ULoadingWidget::GetPaletteCategory()
{
	return LOCTEXT("Palette Category", "HUD Elements");
}

#endif

void ULoadingWidget::NativeConstruct()
{
	Super::NativeConstruct();

	UWidgetAnimation* LoadAnimation = GetLoadingAnimation();
	if (LoadAnimation)
	{
		PlayAnimation(LoadAnimation, 0.f, 0);
	}
}

void ULoadingWidget::NativeDestruct()
{
	Super::NativeDestruct();

	StopAnimation(GetLoadingAnimation());
}

#undef LOCTEXT_NAMESPACE


