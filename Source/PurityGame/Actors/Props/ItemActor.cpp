// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ItemActor.h"

#include "Assets/Interactive/ItemAsset.h"

#include "Characters/Player/Protagonist.h"
#include "Characters/Player/ProtagonistController.h"

AItemActor::AItemActor()
{
	PrimaryActorTick.bCanEverTick = false;

	bCanBeCollected = true;

	// Create the mesh and set it as the root
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Item Mesh"));
	RootComponent = Mesh;
}

void AItemActor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	UStaticMesh* ItemMesh = TYPE_OF_NULLPTR();
	if (Item)
	{
		ItemMesh = Item->GetItemMesh();
	}
	else
	{
		bCanBeCollected = false;
	}

	Mesh->SetStaticMesh(ItemMesh);
}

void AItemActor::OnProtagonistFocus_Implementation(AProtagonistController* Player)
{
	Mesh->SetRenderCustomDepth(true);
}

void AItemActor::OnProtagonistUnfocus_Implementation(AProtagonistController* Player)
{
	Mesh->SetRenderCustomDepth(false);
}

FText AItemActor::GetFocusTip_Implementation() const
{
	if (Item)
	{
		return Item->GetItemName();
	}

	return FText::GetEmpty();
}

void AItemActor::OnProtagonistInteract_Implementation(AProtagonistController* Player)
{
	AProtagonist* Protagonist = Player->GetProtagonist();

	// If this item is added to the inventory, Destroy this actor
	int32 Index;
	if (Protagonist->AddItemToInventory(Item, Index))
	{
		OnPickup(Player);
		OnCollected.Broadcast(Item);
		Destroy();
	}
}

