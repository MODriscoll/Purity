// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "FocusTipWidget.generated.h"

/**
 * Widget that allows C++ classes to
 * display a focus tip to the user. Bind
 * the text widget to utilise the tip
 */
UCLASS(abstract, meta = (DisplayName = "Focus Tip"))
class PURITYGAME_API UFocusTipWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/** The text display to the user */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Tips)
	FText FocusTip;
};
