// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ReadableActor.h"

#include "Widgets/HUD/MessagePopupWidget.h"

#include "Characters/Player/ProtagonistController.h"
#include "Characters/Player/ProtagonistHUD.h"

DEFINE_LOG_CATEGORY(LogReadableActor);

AReadableActor::AReadableActor()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default values
	bInstantlyToggle = true;
	bStallToggleRequest = true;

	// Create the visual mesh and set it as the root
	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = VisualMesh;

	// Don't allow the mesh to apply physics
	VisualMesh->SetSimulatePhysics(false);
}

void AReadableActor::OnProtagonistFocus_Implementation(AProtagonistController* Player)
{
	VisualMesh->SetRenderCustomDepth(true);
}

void AReadableActor::OnProtagonistUnfocus_Implementation(AProtagonistController* Player)
{
	VisualMesh->SetRenderCustomDepth(false);
}

FText AReadableActor::GetFocusTip_Implementation() const
{
	return MessageTitle;
}

void AReadableActor::OnProtagonistInteract_Implementation(AProtagonistController* Player)
{
	CreateMessageWidget(Player);
}

void AReadableActor::CreateMessageWidget(AProtagonistController* Player)
{
	AProtagonistHUD* PlayerHUD = Player->GetProtagonistHUD();

	// Don't overwrite the existing message if any
	if (!MessageReference && PlayerHUD && !PlayerHUD->IsMessageQueued())
	{
		// Make sure we have a valid template to use
		if (!Template)
		{
			UE_LOG(LogReadableActor, Warning, TEXT("Could not create message. "
				"Template for \"%s\" is null"), *GetName());
			return;
		}

		MessageReference = CreateWidget<UMessagePopupWidget>(Player, Template);
		if (MessageReference)
		{
			FMessageSettings Settings;
			Settings.bInstantlyToggle = bInstantlyToggle;
			Settings.bStallToggleRequest = bStallToggleRequest;
			PlayerHUD->ShowMessage(MessageReference, true, Settings);

			MessageReference->SetMessage(MessageImage);

			MessageReference->OnHiddenFinished.AddUFunction(
				this, TEXT("OnMessageClosed"));
		}
	}
}

void AReadableActor::OnMessageClosed()
{
	MessageReference = TYPE_OF_NULLPTR();
}
