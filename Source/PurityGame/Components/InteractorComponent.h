// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "InteractorComponent.generated.h"

/** Delegate for when an interactive has either be focused or unfocused */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractiveFocusSignature, AActor*, AsActor);

DECLARE_LOG_CATEGORY_EXTERN(LogInteractorComponent, Log, All);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PURITYGAME_API UInteractorComponent final : public USceneComponent
{
	GENERATED_BODY()

public:	

	/** Sets default values for this component's properties */
	UInteractorComponent();

public:	

	/** Called every frame */
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override final;

public:

	/** Returns the latest found interactive object if its focusable, this value could be null */
	UFUNCTION(BlueprintCallable, Category = Interaction)
	AActor* GetFoundInteractive() const;

private:

	/** Generates the trace data for the raycast */
	void GetTraceParameters(FVector& OutOrigin, FVector& OutDestination, 
		FCollisionQueryParams& OutParameters, AProtagonistController* Controller);

	/** Resolves the latest trace if an object was hit */
	void ResolveTrace(const FHitResult& TraceResult, AProtagonistController* Controller);

	/** Resolves the conflict between the current found object and a new one 
	 *	@param1: The interactive object as an actor */
	void ResolveConflict(AActor* AsActor, AProtagonistController* Controller);

	/** Resolves the found interactive object if its no longer valid */
	void ResolveInvalidInteractive(AProtagonistController* Controller);

	/** Gets the protagonist controller to send out */
	AProtagonistController* GetInteractingController() const;

public:

	/** Delegate for when a new interactive has been
	focused on by the interacting controller  
		@param1: The interactive as an actor. This
		actor will always implement the OnFocus interface */
	FInteractiveFocusSignature OnObjectFocused;

	/** Delegate for when an old interactive has
	lost focus from the interacting controller 
		@param1: The interactive as an actor. This
		actor will always implement the OnFocus interface */
	FInteractiveFocusSignature OnObjectUnfocused;

	/** The distance from this component objects need to be within before being recognised */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interaction)
	float InteractDistance;

	/** If checks should utilise complex collision */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interaction)
	uint32 bTraceComplex : 1;

	/** If a controller can be found, should the interactor use the controllers rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interaction, 
		meta = (DisplayName = "Use Controller Rotation"))
	uint32 bUsePawnControlRotation : 1;

	/** If interaction should still be applied if the owner
	has a controller and the controller is in cinematic mode */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interaction,
		meta = (DisplayName = "Allow Interaction in Cinematic Mode"))
	uint32 bKeepFocusInCinematicMode : 1;

private:

	/** The latest interactive object to be found as an actor 
	This actor will always implement the focuson interface */
	UPROPERTY(Transient)
	AActor* FoundInteractive;
};

