// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "MapPopupWidget.h"

#include "Components/CanvasPanelSlot.h"

#include "MinimapWidget.h"

void UMapPopupWidget::SetMinimapBackdrop(UTexture2D* BackdropTexture)
{
	UMinimapWidget* Minimap = GetMinimapWidget();
	if (Minimap)
	{
		// Set the backdrop size to fit inside the minimap space
		UCanvasPanelSlot* MinimapSlot = Cast<UCanvasPanelSlot>(Minimap->Slot);
		if (MinimapSlot)
		{
			Minimap->SetMapBackdropWithSize(BackdropTexture, MinimapSlot->GetSize());
		}
		else
		{
			Minimap->SetMapBackdrop(BackdropTexture);
		}
	}
}