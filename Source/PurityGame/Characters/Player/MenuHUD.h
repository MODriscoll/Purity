// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UMainMenuWidget;
class USubMenuWidget;

class AMainMenuGameModeBase;

#include "GameFramework/HUD.h"
#include "MenuHUD.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMenuHUD, Log, All);

/**
 * A HUD designed for use by menu related widgets. Controls
 * and maintains a simple widget designed for UI only input.
 */
UCLASS()
class PURITYGAME_API AMenuHUD : public AHUD
{
	GENERATED_BODY()
	
protected:

	/** Called after actor is created and spawned */
	virtual void BeginPlay() override;

public:

	/** Give the main menu widget reference to the game mode */
	void SetMainMenuReference(AMainMenuGameModeBase* MainMenu);

	/** Creates and immediately adds a new sub menu to the viewport */
	UFUNCTION(BlueprintCallable, Category = "HUD")
	USubMenuWidget* CreateSubMenu(TSubclassOf<USubMenuWidget> Template, float Alpha = 0.f);

	/** Clears the sub menu being shown if any */
	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ClearSubMenu();

	/** Get the main menu widget */
	UMainMenuWidget* GetMainMenuWidget() const { return MainMenuReference; }

private:

	/** Creates the menu widget */
	bool CreateMenuWidget();

public:
	
	/** The template for the menu widget to manage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
	TSubclassOf<UMainMenuWidget> MenuTemplate;

private:

	/** Reference to the main menu widget */
	UPROPERTY(BlueprintReadOnly, Transient,
		meta = (AllowPrivateAccess = "true"))
	UMainMenuWidget* MainMenuReference;

	/** Reference to the current sub menu being displayed */
	UPROPERTY(Transient)
	USubMenuWidget* SubMenuReference;
};
