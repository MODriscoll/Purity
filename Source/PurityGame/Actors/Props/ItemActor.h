// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UItemAsset;

#include "GameFramework/Actor.h"
#include "Interfaces/ProtagonistCanFocusOn.h"
#include "Interfaces/ProtagonistCanInteractWith.h"
#include "ItemActor.generated.h"

/** Called when the item has been successfully picked up by the protagonist */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemCollectedSignature, UItemAsset* const, Item);

UCLASS()
class PURITYGAME_API AItemActor : public AActor, public IProtagonistCanFocusOn, public IProtagonistCanInteractWith
{
	GENERATED_BODY()
	
public:	

	/** Sets default values for this actor's properties */
	AItemActor();

public:

	/** Sets the mesh of the actor if item has been set */
	virtual void OnConstruction(const FTransform& Transform) override;

	/** Highlights this item in the world */
	virtual void OnProtagonistFocus_Implementation(AProtagonistController* Player) override;

	/** Unhighlights this item in the world */
	virtual void OnProtagonistUnfocus_Implementation(AProtagonistController* Player) override;

	/** Only allow items to be focused on when they can be collected */
	virtual bool CanBeFocused_Implementation() const override { return bCanBeCollected; }

	/** Informs the protagonist of what this item is */
	virtual FText GetFocusTip_Implementation() const override;

	/** Tries to add this item to the players inventory.
	If successfully added, this actor will destroy itself */
	virtual void OnProtagonistInteract_Implementation(AProtagonistController* Player) override;

protected:

	/** Called when the protagonist successfully picked up this item */
	UFUNCTION(BlueprintNativeEvent, Category = "Item")
	void OnPickup(AProtagonistController* Player);
	virtual void OnPickup_Implementation(AProtagonistController* Player) { }

public:

	/** Delegate for when this item was
	successfully collected by the player
		@param1: The item the player picked up */
	UPROPERTY(BlueprintAssignable, Category = "Item")
	FItemCollectedSignature OnCollected;

	/** If this item can is able to be collected by the player */
	UPROPERTY(Interp, BlueprintReadWrite, Category = "Item",
		meta = (EditCondition = "!!Item"))
	uint32 bCanBeCollected : 1;

protected:

	/** The mesh representing this item */
	UPROPERTY(Category = "Item", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;

	/** The item this actor is representing */
	UPROPERTY(Category = "Item", EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UItemAsset* Item;
};
