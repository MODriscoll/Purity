// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ButtonActor.h"

#include "Characters/Player/ProtagonistController.h"

DEFINE_LOG_CATEGORY(LogButtonActor);

AButtonActor::AButtonActor()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default variables
	bCanBePressed = true;
	bAutoDisable = false;
	bShouldDelay = true;
	DelayFor = 0.2f;

	// Create the mesh for the button and set it as the root
	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Button Mesh"));
	RootComponent = ButtonMesh;

	// Set the button to not simulate physics
	ButtonMesh->SetSimulatePhysics(false);
}

void AButtonActor::OnProtagonistFocus_Implementation(AProtagonistController* Player)
{
	ButtonMesh->SetRenderCustomDepth(true);
}

void AButtonActor::OnProtagonistUnfocus_Implementation(AProtagonistController* Player)
{
	ButtonMesh->SetRenderCustomDepth(false);
}

#define LOCTEXT_NAMESPACE "Protagonist Focusable"

FText AButtonActor::GetFocusTip_Implementation() const
{
	return LOCTEXT("Focusable", "Press Button");
}

#undef LOCTEXT_NAMESPACE

void AButtonActor::OnProtagonistInteract_Implementation(AProtagonistController* Player)
{
	Press(Player);
}

void AButtonActor::Press(AActor* PressedBy)
{
	bool bWasAccepted = false;
	if (bCanBePressed)
	{
		bWasAccepted = true;
		if (bAutoDisable)
		{
			SetCanBePressed(false, bShouldDelay);
		}

		OnButtonPressAccepted.Broadcast(this, PressedBy);
	}
	else
	{
		OnButtonPressDenied.Broadcast(this, PressedBy);
	}
}

void AButtonActor::SetCanBePressed(bool bCan, bool bDelay)
{
	bool bPrevious = bCanBePressed;
	bCanBePressed = bCan;

	FTimerManager& Manager = GetWorldTimerManager();
	if (bCan)
	{
		// Remove the handle from the timer if active
		if (Manager.IsTimerActive(DelayHandle))
		{
			Manager.ClearTimer(DelayHandle);
		}

		// If button was previously delayed, call activation event
		if (!bPrevious)
		{
			OnButtonActivated.Broadcast();
		}
	}
	// Only delay if requested
	else if (bShouldDelay)
	{
		// Make sure door isn't already delayed
		if (!Manager.IsTimerActive(DelayHandle))
		{
			Manager.SetTimer(DelayHandle, this, &AButtonActor::Activate, DelayFor, false);
		}
	}
}

void AButtonActor::Activate()
{
	SetCanBePressed(true);
}



