// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "ElevatorActor.h"

#include "Actors/Props/Buttons/ElevatorButtonActor.h"
#include "Actors/Props/DoorActor.h"

DEFINE_LOG_CATEGORY(LogElevatorActor);

AElevatorActor::AElevatorActor()
{
	PrimaryActorTick.bCanEverTick = false;

	// Set default values
	StartAtFloor = 0;
	Speed = 500.f;
	FloorLevels.Add(FVector::ZeroVector);

	// Create the base of the elevator and set it as the root
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Elevator Base"));
	RootComponent = BaseMesh;

	// Set the elevator to not simulate physics
	BaseMesh->SetSimulatePhysics(false);
}

void AElevatorActor::BeginPlay()
{
	Super::BeginPlay();

	CalculateWorldFloorLocations();

	TransitionFromIndex = TransitionToIndex;
	TransitionState = EElevatorState::Waiting;

	// This elevator is designed to work with doors but
	// doesn't need to. We should still warn incase 
	if (ElevatorDoor)
	{
		ElevatorDoor->OnDoorOpened.AddDynamic(this, &AElevatorActor::FinishTransition);
		ElevatorDoor->OnDoorClosed.AddDynamic(this, &AElevatorActor::StartElevation);
		ElevatorDoor->bIsCosmetic = false;
	}
	else
	{
		UE_LOG(LogElevatorActor, Warning, TEXT("No doors have been set for \"%s\""), *GetName());
	}

	// The elevator requires buttons, check in-case
	for (AElevatorButtonActor* Button : ElevatorButtons)
	{
		if (Button)
		{
			Button->OnButtonPressAccepted.AddDynamic(this, &AElevatorActor::PanelButtonPressed);
		}
		else
		{
			UE_LOG(LogElevatorActor, Warning, TEXT("Elevator \"%s\" contains null "
				"button reference. You might be missing a button"), *GetName());
		}
	}
}

void AElevatorActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (ElevatorDoor)
	{
		ElevatorDoor->OnDoorOpened.RemoveDynamic(this, &AElevatorActor::FinishTransition);
		ElevatorDoor->OnDoorClosed.RemoveDynamic(this, &AElevatorActor::StartElevation);
	}

	for (AElevatorButtonActor* Button : ElevatorButtons)
	{
		if (Button)
		{
			Button->OnButtonPressAccepted.RemoveDynamic(this, &AElevatorActor::PanelButtonPressed);
		}
		else
		{
			UE_LOG(LogElevatorActor, Warning, TEXT("Elevator \"%s\" contains null "
				"button reference. You might be missing a button"), *GetName());
		}
	}
}

void AElevatorActor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	// Elevators should require at least one level
	if (FloorLevels.Num() >= 1)
	{
		FloorLevels[0] = FVector::ZeroVector;
	}
}

void AElevatorActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Assure were are actually transitioning
	if (TransitionState == EElevatorState::Transitioning)
	{
		bool bFinished = Elevate(DeltaSeconds);

		if (bFinished)
		{
			FinishElevation();
		}
	}
}

bool AElevatorActor::GotoFloor(int32 FloorIndex)
{
	bool bIsValid = !IsTransitioning();

	if (bIsValid)
	{
		// We don't need to transition if floor index
		// is requesting the floor we are already at
		bIsValid = TransitionToIndex != FloorIndex;
	}

	// Only start transition if still valid
	if (bIsValid)
	{
		StartTransition(FloorIndex);
	}

	return bIsValid;
}

bool AElevatorActor::GetEstimatedFloorIndex(int32& OutEstimatedIndex) const
{
	bool bIsTransitioning = IsTransitioning();

	if (!bIsTransitioning)
	{
		OutEstimatedIndex = TransitionToIndex;
	}
	else
	{
		// Find the floor above the elevator
		const FVector CurrentLocation = GetActorLocation();
		int32 FloorAboveIndex = FloorLevels.IndexOfByPredicate(
			[CurrentLocation](const FVector& Floor)->bool { return CurrentLocation.Z <= Floor.Z; });

		if (FloorAboveIndex != INDEX_NONE)
		{
			if (FloorAboveIndex > 0)
			{
				const FVector& FloorAbove = FloorLevels[FloorAboveIndex];
				const FVector& FloorBelow = FloorLevels[FloorAboveIndex - 1];

				// If the elevators distance is below the half distance, it means
				// the elevator is closer to the floor below than the floor above
				float DistancedHalf = (FloorAbove.Z - FloorBelow.Z) * 0.5f;
				float ElevatorDistance = CurrentLocation.Z - FloorBelow.Z;

				if (DistancedHalf > ElevatorDistance)
				{
					OutEstimatedIndex = FloorAboveIndex - 1;
				}
				else
				{
					OutEstimatedIndex = FloorAboveIndex;
				}		
			}
			else
			{
				OutEstimatedIndex = FloorAboveIndex;
			}
		}
		else
		{
			UE_LOG(LogElevatorActor, Warning,
				TEXT("\"%s\" could not estimate floor index, elevator might be out of range"), *GetName())
			OutEstimatedIndex = INDEX_NONE;
		}
	}

	return bIsTransitioning;
}

void AElevatorActor::CalculateWorldFloorLocations()
{
	if (FloorLevels.Num() > 0)
	{
		// Convert all the floor locations from relative to world
		{
			FTransform BaseTransform = GetActorTransform();
			FVector BaseLocation = BaseTransform.GetLocation();

			for (FVector& FloorLocation : FloorLevels)
			{
				FloorLocation = BaseTransform.TransformPositionNoScale(FloorLocation);

				// Keep floor align with elevators base XY location
				FloorLocation.X = BaseLocation.X;
				FloorLocation.Y = BaseLocation.Y;
			}
		}

		if (FloorLevels.IsValidIndex(StartAtFloor))
		{
			SetActorLocation(FloorLevels[StartAtFloor]);
			TransitionToIndex = StartAtFloor;
		}
	}
	else
	{
		TransitionToIndex = 0;
	}

	TransitionFromIndex = TransitionToIndex;
}

void AElevatorActor::StartTransition(int32 FloorIndex)
{
	ToggleButtons(false);

	TransitionFromIndex = TransitionToIndex;
	TransitionToIndex = FloorIndex;

	// Set state here in-case skipping will happen
	TransitionState = EElevatorState::PreTransition;

	// If no doors are set, skip straight to elevation.
	if (!ElevatorDoor)
	{
		StartElevation();
	}
	// Also skip if the doors are already closed. If they
	// are closing now, just wait for them to close.
	else if (!ElevatorDoor->IsOpen())
	{
		if (!ElevatorDoor->IsSwinging())
		{
			StartElevation();
		}
	}
	else
	{ 
		// Call the state change event now,
		// as it hasn't been called yet
		SetState(EElevatorState::PreTransition);

		ElevatorDoor->Close(this);
	}
}

void AElevatorActor::StartElevation()
{
	// If the door automatically closed, we should still be waiting
	// So only continue if we are in pre transition phase
	if (!IsTransitioning())
	{
		return;
	}

	// Elevator needs to tick in-order to elevate
	SetActorTickEnabled(true);

	SetState(EElevatorState::Transitioning);
}

void AElevatorActor::FinishTransition()
{
	if (IsTransitioning())
	{
		SetState(EElevatorState::Waiting);
	}

	ToggleButtons(true);
}

void AElevatorActor::FinishElevation()
{
	SetActorTickEnabled(false);

	// If no doors are set, finish the transition now
	if (!ElevatorDoor)
	{
		FinishTransition();
		return;
	}

	ElevatorDoor->Open(this);
	SetState(EElevatorState::PostTransition);
}

bool AElevatorActor::Elevate(float DeltaSeconds)
{
	// Calculate the delta to move by
	float Delta = Speed * DeltaSeconds;
	
	FVector NewLocation = GetActorLocation();
	const FVector Displacement = FloorLevels[TransitionToIndex] - NewLocation;

	// Make sure we don't overshoot in the direction we are going in
	// If delta is greater, we are in range of finishing the transition
	bool bInRange = false;
	if (Delta < FMath::Abs(Displacement.Z))
	{
		Delta *= FMath::Sign(Displacement.Z);
	}
	else
	{
		Delta = Displacement.Z;
		bInRange = true;
	}

	// Get elevator to sweep to pickup up the player incase delta is too big
	NewLocation.Z += Delta;
	SetActorLocation(NewLocation, true);

	return bInRange;
}

void AElevatorActor::SetState(EElevatorState NewState)
{
	TransitionState = NewState;
#if WITH_EDITOR
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EElevatorState"), true);
	UE_LOG(LogElevatorActor, Log, TEXT("\"%s\" has switch to state: %s"), 
		*GetName(), EnumPtr ? *EnumPtr->GetEnumName(static_cast<int32>(TransitionState)) : TEXT("Invalid"));
#endif
	OnElevatorStateChanged.Broadcast(TransitionState);
}

void AElevatorActor::PanelButtonPressed(AButtonActor* Button, AActor* PressedBy)
{
	AElevatorButtonActor* PressedButton = Cast<AElevatorButtonActor>(Button);
	if (PressedButton)
	{
		// If the elevator is already at this floor,
		// open their doors because they might be closed
		if (PressedButton->FloorIndex == TransitionToIndex)
		{
			if (ElevatorDoor && !IsTransitioning())
			{
				ElevatorDoor->Open(this);
			}
		}
		else
		{
			GotoFloor(PressedButton->FloorIndex);
		}
	}
}

void AElevatorActor::ToggleButtons(bool bEnable)
{
	for (AElevatorButtonActor* Button : ElevatorButtons)
	{
		if (Button)
		{
			Button->SetCanBePressed(bEnable);
		}
		else
		{
			UE_LOG(LogElevatorActor, Warning, TEXT("Elevator \"%s\" contains null "
				"button reference. You might be missing a button"), *GetName());
		}
	}
}

