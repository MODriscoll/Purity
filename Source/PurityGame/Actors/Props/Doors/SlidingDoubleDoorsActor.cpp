// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "SlidingDoubleDoorsActor.h"

ASlidingDoubleDoorsActor::ASlidingDoubleDoorsActor()
{
	PrimaryActorTick.bCanEverTick = false;

	MaxOffset = 300.f;

	// Create the base of the door and set it as the root
	DoorBase = CreateDefaultSubobject<UArrowComponent>(TEXT("Door Base"));
	RootComponent = DoorBase;

	// Create the left and right door, attach both to the base
	NegativeDoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Negative Door"));
	PositiveDoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Positive Door"));
	NegativeDoorMesh->SetupAttachment(DoorBase);
	PositiveDoorMesh->SetupAttachment(DoorBase);

	// Set both doors to not simulate physics
	NegativeDoorMesh->SetSimulatePhysics(false);
	PositiveDoorMesh->SetSimulatePhysics(false);

	// Set the door to slide along the X axis by default
	SetSlideAxis(EDoorSlideAxis::X);
}

#if WITH_EDITOR

void ASlidingDoubleDoorsActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UWorld* World = GetWorld();

	FName PropertyName = PropertyChangedEvent.Property ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(ASlidingDoubleDoorsActor, MaxOffset))
	{
		// Only open the doors if in the editor
		if (!World->IsGameWorld())
		{
			TickDoorMotion(1.f);
		}
	}
	else if (PropertyName == GET_MEMBER_NAME_CHECKED(ASlidingDoubleDoorsActor, SlideAxis))
	{
		SetSlideAxis(SlideAxis);
		
		// Only open the doors if in the editor
		if (!World->IsGameWorld())
		{
			TickDoorMotion(1.f);
		}
	}
}

#endif

void ASlidingDoubleDoorsActor::TickDoorMotion(float Alpha)
{
	float ClampedAlpha = FMath::Clamp(Alpha, 0.f, 1.f);

	FVector Offset = FVector::ZeroVector;
	switch (SlideAxis)
	{
		case EDoorSlideAxis::X:
		{
			Offset.X = MaxOffset;
			break;
		}
		case EDoorSlideAxis::Z:
		{
			Offset.Z = MaxOffset;
			break;
		}
	}

	// Slide the doors away from the base using the alpha
	FVector NewNegativeOffset = FMath::Lerp(FVector::ZeroVector, Offset * -1.f, ClampedAlpha);
	FVector NewPositiveOffset = FMath::Lerp(FVector::ZeroVector, Offset, ClampedAlpha);
	NegativeDoorMesh->SetRelativeLocation(NewNegativeOffset);
	PositiveDoorMesh->SetRelativeLocation(NewPositiveOffset);
}

void ASlidingDoubleDoorsActor::SetSlideAxis(EDoorSlideAxis Axis)
{
	SlideAxis = Axis;

	switch (SlideAxis)
	{
		case EDoorSlideAxis::X:
		{
			PositiveDoorMesh->SetRelativeRotation(FRotator(0.f));
			NegativeDoorMesh->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));
			break;
		}
		case EDoorSlideAxis::Z:
		{
			PositiveDoorMesh->SetRelativeRotation(FRotator(0.f));
			NegativeDoorMesh->SetRelativeRotation(FRotator(0.f, 0.f, 180.f));
			break;
		}
	}
}


