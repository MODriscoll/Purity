// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UInventoryWidget;
class UMinimapWidget;

#include "PopupWidget.h"
#include "MapPopupWidget.generated.h"

/**
 * Widget that allows C++ classes to interact with
 * the Map HUD. Provides access to the minimap as
 * well as the inventory widget. 
*/
UCLASS(abstract, meta = (DisplayName = "Map HUD"))
class PURITYGAME_API UMapPopupWidget : public UPopupWidget
{
	GENERATED_BODY()

public:

	/** Sets the backdrop for the minimap, use this 
	function instead of setting minimap backdrop directly */
	UFUNCTION(BlueprintCallable, Category = Minimap)
	void SetMinimapBackdrop(UTexture2D* BackdropTexture);

	/** Get the minimap associated with this map */
	UFUNCTION(BlueprintImplementableEvent, BlueprintPure, Category = "Map HUD",
		meta = (DisplayName = "Get Minimap"))
	UMinimapWidget* GetMinimapWidget() const;

	/** Get the inventory associated with this map */
	UFUNCTION(BlueprintImplementableEvent, BlueprintPure, Category = "Map HUD",
		meta = (DisplayName = "Get Inventory"))
	UInventoryWidget* GetInventoryWidget() const;
};
