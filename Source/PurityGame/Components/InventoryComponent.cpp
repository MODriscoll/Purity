// Fill out your copyright notice in the Description page of Project Settings.

#include "PurityGame.h"
#include "InventoryComponent.h"

#include "Assets/Interactive/ItemAsset.h"

DEFINE_LOG_CATEGORY(LogInventoryComponent);

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	MaxSize = 5;
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	StoredItems.Init(FInventorySlot(), MaxSize);
}

bool UInventoryComponent::AddItem(UItemAsset* ItemToAdd, int32& OutIndex)
{
	if (!ItemToAdd)
	{
		UE_LOG(LogInventoryComponent, Warning, TEXT("Item being added to inventory is null"));
		return false;
	}

	OutIndex = -1;

	for (int32 i = 0; i < StoredItems.Num(); ++i)
	{
		FInventorySlot& Slot = StoredItems[i];
		if (Slot.ItemReference == TYPE_OF_NULLPTR())
		{
			Slot.ItemReference = ItemToAdd;
			OnItemAdded.Broadcast(ItemToAdd, i);
			OutIndex = i;
			break;
		}
	}

	return OutIndex != -1;
}

bool UInventoryComponent::QueryItems(FInventoryQuery& InOutQuery)
{
	InOutQuery.ItemsFound.Empty();
	InOutQuery.ItemsMissing.Empty();

	// Get the amount of query items to compare when query is finished
	int32 Requirements = InOutQuery.ItemsToQuery.Num();

	// Make a clone of the inventory, as both the items to query
	// and the clone will have items removed from the array
	TArray<FInventorySlot> InventoryClone = StoredItems;

	for (int32 i = Requirements - 1; i >= 0; --i)
	{
		// Remove this item from the query after 
		// getting a copy of the current item
		UItemAsset* RequiredItem = InOutQuery.ItemsToQuery[i];
		InOutQuery.ItemsToQuery.RemoveAt(i);

		bool ItemFound = false;

		for (int32 j = InventoryClone.Num() - 1; j >= 0; --j)
		{
			if (RequiredItem == InventoryClone[j].ItemReference)
			{
				// Add this item to the found list after
				// removing it from the query search
				InventoryClone.RemoveAt(j);
				InOutQuery.ItemsFound.Add(RequiredItem);
				ItemFound = true;
				break;
			}
		}

		// If the item wasn't found, add it to the missing list
		if (!ItemFound)
		{
			InOutQuery.ItemsMissing.Add(RequiredItem);
		}
	}

	// If all items were found, requirements 
	// will equal the size of the found list
	return InOutQuery.ItemsFound.Num() == Requirements;
}

bool UInventoryComponent::ContainsItem(const FText& ItemName)
{
	FInventorySlot* const ItemSlot = StoredItems.FindByPredicate(
		[&ItemName](const FInventorySlot& Slot)->bool 
	{ 
		if (Slot.ItemReference)
		{
			const FString QueryItemName = Slot.ItemReference->GetItemName().ToString();
			return ItemName.ToString() == QueryItemName;
		}

		return false;
	});

	return !!ItemSlot;
}

bool UInventoryComponent::RemoveItem(int32 Index)
{
	if (StoredItems.IsValidIndex(Index))
	{
		FInventorySlot& Slot = StoredItems[Index];
		UItemAsset* Item = Slot.ItemReference;
		Slot.ItemReference = TYPE_OF_NULLPTR();
		OnItemRemoved.Broadcast(Item, Index);
		return true;
	}

	return false;
}

